--DROP SCHEMA gestion CASCADE;

CREATE SCHEMA IF NOT EXISTS gestion AUTHORIZATION suti_admin;


CREATE SEQUENCE gestion.estados_civiles_id_estado_civil_seq_1;

CREATE TABLE gestion.estados_civiles (
                id_estado_civil INTEGER NOT NULL DEFAULT nextval('gestion.estados_civiles_id_estado_civil_seq_1'),
                descripcion VARCHAR NOT NULL,
                CONSTRAINT estados_civiles_pk PRIMARY KEY (id_estado_civil)
);


ALTER SEQUENCE gestion.estados_civiles_id_estado_civil_seq_1 OWNED BY gestion.estados_civiles.id_estado_civil;

CREATE SEQUENCE gestion.ubicaciones_id_ubicacion_seq_1;

CREATE TABLE gestion.ubicaciones (
                id_ubicacion INTEGER NOT NULL DEFAULT nextval('gestion.ubicaciones_id_ubicacion_seq_1'),
                pais VARCHAR NOT NULL,
                departamento VARCHAR NOT NULL,
                ciudad VARCHAR NOT NULL,
                barrio VARCHAR,
                CONSTRAINT ubicaciones_pk PRIMARY KEY (id_ubicacion)
);


ALTER SEQUENCE gestion.ubicaciones_id_ubicacion_seq_1 OWNED BY gestion.ubicaciones.id_ubicacion;

CREATE TABLE gestion.tipos_anotaciones (
                id_tipo_anotacion INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT tipos_anotaciones_pk PRIMARY KEY (id_tipo_anotacion)
);


CREATE TABLE gestion.roles (
                id_roles INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT roles_pk PRIMARY KEY (id_roles)
);


CREATE TABLE gestion.permisos (
                id_permiso INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT permisos_pk PRIMARY KEY (id_permiso)
);


CREATE SEQUENCE gestion.roles_permisos_id_rol_permiso_seq;

CREATE TABLE gestion.roles_permisos (
                id_rol_permiso INTEGER NOT NULL DEFAULT nextval('gestion.roles_permisos_id_rol_permiso_seq'),
                id_roles INTEGER NOT NULL,
                id_permiso INTEGER NOT NULL,
                habilitado BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                fecha_actualizacion TIMESTAMP,
                CONSTRAINT roles_permisos_pk PRIMARY KEY (id_rol_permiso)
);


ALTER SEQUENCE gestion.roles_permisos_id_rol_permiso_seq OWNED BY gestion.roles_permisos.id_rol_permiso;

CREATE TABLE gestion.categorias (
                id_categoria INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT categorias_pk PRIMARY KEY (id_categoria)
);


CREATE TABLE gestion.salas (
                id_sala INTEGER NOT NULL,
                descripcion VARCHAR,
                numero_sala VARCHAR NOT NULL,
                habilitado BOOLEAN NOT NULL,
                CONSTRAINT salas_pk PRIMARY KEY (id_sala)
);


CREATE TABLE gestion.depositos (
                id_deposito INTEGER NOT NULL,
                codigo VARCHAR NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT depositos_pk PRIMARY KEY (id_deposito)
);


CREATE TABLE gestion.tipos_articulo (
                id_tipo_articulo INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT tipos_articulo_pk PRIMARY KEY (id_tipo_articulo)
);


CREATE SEQUENCE gestion.articulos_id_articulo_seq_1;

CREATE TABLE gestion.articulos (
                id_articulo INTEGER NOT NULL DEFAULT nextval('gestion.articulos_id_articulo_seq_1'),
                codigo VARCHAR NOT NULL,
                id_tipo_articulo INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT articulos_pk PRIMARY KEY (id_articulo)
);


ALTER SEQUENCE gestion.articulos_id_articulo_seq_1 OWNED BY gestion.articulos.id_articulo;

CREATE SEQUENCE gestion.depositos_articulos_id_deposito_articulo_seq;

CREATE TABLE gestion.depositos_articulos (
                id_deposito_articulo INTEGER NOT NULL DEFAULT nextval('gestion.depositos_articulos_id_deposito_articulo_seq'),
                id_articulo INTEGER NOT NULL,
                id_deposito INTEGER NOT NULL,
                fecha_vencimiento TIMESTAMP NOT NULL,
                cantidad INTEGER NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                fecha_actualizacion TIMESTAMP,
                CONSTRAINT depositos_articulos_pk PRIMARY KEY (id_deposito_articulo)
);


ALTER SEQUENCE gestion.depositos_articulos_id_deposito_articulo_seq OWNED BY gestion.depositos_articulos.id_deposito_articulo;

CREATE TABLE gestion.unidades (
                id_unidad_medida INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT unidades_pk PRIMARY KEY (id_unidad_medida)
);


CREATE SEQUENCE gestion.items_id_item_seq_1_1;

CREATE TABLE gestion.items (
                id_item INTEGER NOT NULL DEFAULT nextval('gestion.items_id_item_seq_1_1'),
                id_categoria INTEGER NOT NULL,
                id_unidad_medida INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT items_pk PRIMARY KEY (id_item)
);


ALTER SEQUENCE gestion.items_id_item_seq_1_1 OWNED BY gestion.items.id_item;

CREATE SEQUENCE gestion.turnos_id_turno_seq;

CREATE TABLE gestion.turnos (
                id_turno INTEGER NOT NULL DEFAULT nextval('gestion.turnos_id_turno_seq'),
                dia_semana VARCHAR NOT NULL,
                hora_inicio TIME NOT NULL,
                hora_fin TIME NOT NULL,
                CONSTRAINT turnos_pk PRIMARY KEY (id_turno)
);


ALTER SEQUENCE gestion.turnos_id_turno_seq OWNED BY gestion.turnos.id_turno;

CREATE TABLE gestion.tipo_internacion (
                id_tipo_internacion INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT tipo_internacion_pk PRIMARY KEY (id_tipo_internacion)
);


CREATE TABLE gestion.tipos_clientes (
                id_tipo_cliente NUMERIC NOT NULL,
                descripcion VARCHAR NOT NULL,
                CONSTRAINT tipos_clientes_pk PRIMARY KEY (id_tipo_cliente)
);


CREATE SEQUENCE gestion.personas_cod_persona_seq;

CREATE TABLE gestion.personas (
                id_persona INTEGER NOT NULL DEFAULT nextval('gestion.personas_cod_persona_seq'),
                id_paciente VARCHAR NOT NULL,
                nombre VARCHAR NOT NULL,
                apellido VARCHAR NOT NULL,
                fecha_nacimiento TIMESTAMP,
                direccion VARCHAR,
                id_ubicacion INTEGER,
                id_estado_civil INTEGER,
                cantidad_hijos INTEGER NOT NULL,
                observacion VARCHAR,
                CONSTRAINT personas_pk PRIMARY KEY (id_persona)
);


ALTER SEQUENCE gestion.personas_cod_persona_seq OWNED BY gestion.personas.id_persona;

CREATE SEQUENCE gestion.turno_enfermero_id_turno_enfermero_seq;

CREATE TABLE gestion.turno_enfermero (
                id_turno_enfermero INTEGER NOT NULL DEFAULT nextval('gestion.turno_enfermero_id_turno_enfermero_seq'),
                id_enfermero INTEGER NOT NULL,
                id_turno INTEGER NOT NULL,
                habilitado BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                CONSTRAINT turno_enfermero_pk PRIMARY KEY (id_turno_enfermero)
);


ALTER SEQUENCE gestion.turno_enfermero_id_turno_enfermero_seq OWNED BY gestion.turno_enfermero.id_turno_enfermero;

CREATE SEQUENCE gestion.roles_personas_id_rol_persona_seq;

CREATE TABLE gestion.roles_personas (
                id_rol_persona INTEGER NOT NULL DEFAULT nextval('gestion.roles_personas_id_rol_persona_seq'),
                id_roles INTEGER NOT NULL,
                id_persona INTEGER NOT NULL,
                habilitado BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                fecha_actualizacion TIMESTAMP,
                CONSTRAINT roles_personas_pk PRIMARY KEY (id_rol_persona)
);


ALTER SEQUENCE gestion.roles_personas_id_rol_persona_seq OWNED BY gestion.roles_personas.id_rol_persona;

CREATE TABLE gestion.internados (
                id_internado INTEGER NOT NULL,
                id_persona INTEGER NOT NULL,
                id_sala INTEGER NOT NULL,
                id_tipo_internacion INTEGER NOT NULL,
                id_tipo_cliente NUMERIC NOT NULL,
                fecha_inicio TIMESTAMP NOT NULL,
                fecha_fin TIMESTAMP,
                fecha_fin_probable TIMESTAMP NOT NULL,
                nro_expediente NUMERIC NOT NULL,
                id_sucursal INTEGER NOT NULL,
                reingreso BOOLEAN NOT NULL,
                seguro_medico VARCHAR,
                CONSTRAINT internados_pk PRIMARY KEY (id_internado)
);


CREATE SEQUENCE gestion.anotaciones_id_nota_seq;

CREATE TABLE gestion.anotaciones (
                id_anotacion INTEGER NOT NULL DEFAULT nextval('gestion.anotaciones_id_nota_seq'),
                id_registrador INTEGER NOT NULL,
                id_tipo_anotacion INTEGER NOT NULL,
                valor VARCHAR NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                fecha_actualizacion TIMESTAMP NOT NULL,
                id_internado INTEGER NOT NULL,
                CONSTRAINT anotaciones_pk PRIMARY KEY (id_anotacion)
);


ALTER SEQUENCE gestion.anotaciones_id_nota_seq OWNED BY gestion.anotaciones.id_anotacion;

CREATE SEQUENCE gestion.desechables_id_desechable_utilizado_seq;

CREATE TABLE gestion.desechables (
                id_desechable_utilizado INTEGER NOT NULL DEFAULT nextval('gestion.desechables_id_desechable_utilizado_seq'),
                id_internado INTEGER NOT NULL,
                id_articulo INTEGER NOT NULL,
                id_enfermero INTEGER NOT NULL,
                id_turno_enfermero INTEGER NOT NULL,
                cantidad INTEGER NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                CONSTRAINT desechables_pk PRIMARY KEY (id_desechable_utilizado)
);


ALTER SEQUENCE gestion.desechables_id_desechable_utilizado_seq OWNED BY gestion.desechables.id_desechable_utilizado;

CREATE SEQUENCE gestion.medicos_asignados_id_medico_asignado_seq;

CREATE TABLE gestion.medicos_asignados (
                id_medico_asignado INTEGER NOT NULL DEFAULT nextval('gestion.medicos_asignados_id_medico_asignado_seq'),
                id_medico INTEGER NOT NULL,
                id_internado INTEGER NOT NULL,
                habilitado BOOLEAN DEFAULT TRUE NOT NULL,
                fecha_asignacion TIMESTAMP NOT NULL,
                fecha_actualizacion TIMESTAMP,
                CONSTRAINT medicos_asignados_pk PRIMARY KEY (id_medico_asignado)
);


ALTER SEQUENCE gestion.medicos_asignados_id_medico_asignado_seq OWNED BY gestion.medicos_asignados.id_medico_asignado;

CREATE SEQUENCE gestion.medicamento_internado_id_medicamento_internado_seq;

CREATE TABLE gestion.medicamento_internado (
                id_medicamento_internado INTEGER NOT NULL DEFAULT nextval('gestion.medicamento_internado_id_medicamento_internado_seq'),
                id_medicamento INTEGER NOT NULL,
                id_unidad_medida INTEGER NOT NULL,
                cantidad_asignada REAL NOT NULL,
                cantidad_utilizada REAL NOT NULL,
                fecha_asignacion TIMESTAMP NOT NULL,
                id_internado INTEGER NOT NULL,
                CONSTRAINT medicamento_internado_pk PRIMARY KEY (id_medicamento_internado)
);


ALTER SEQUENCE gestion.medicamento_internado_id_medicamento_internado_seq OWNED BY gestion.medicamento_internado.id_medicamento_internado;

CREATE SEQUENCE gestion.medicamento_suministrado_id_suministro_seq_1;

CREATE TABLE gestion.medicamento_suministrado (
                id_medicamento_suministrado INTEGER NOT NULL DEFAULT nextval('gestion.medicamento_suministrado_id_suministro_seq_1'),
                id_medicamento_internado INTEGER NOT NULL,
                fecha_hora TIMESTAMP NOT NULL,
                cantidad REAL NOT NULL,
                CONSTRAINT medicamento_suministrado_pk PRIMARY KEY (id_medicamento_suministrado)
);


ALTER SEQUENCE gestion.medicamento_suministrado_id_suministro_seq_1 OWNED BY gestion.medicamento_suministrado.id_medicamento_suministrado;

CREATE SEQUENCE gestion.registro_medico_id_registro_seq;

CREATE TABLE gestion.registro_medico (
                id_registro INTEGER NOT NULL DEFAULT nextval('gestion.registro_medico_id_registro_seq'),
                id_registrador INTEGER NOT NULL,
                id_turno_enfermero INTEGER NOT NULL,
                valor_registro VARCHAR NOT NULL,
                fecha_insercion TIMESTAMP DEFAULT CURRENT_TIMESTAMP NOT NULL,
                fecha_actualizacion TIMESTAMP NOT NULL,
                id_medicamento_suministrado INTEGER,
                id_internado INTEGER NOT NULL,
                id_item INTEGER NOT NULL,
                CONSTRAINT registro_medico_pk PRIMARY KEY (id_registro)
);


ALTER SEQUENCE gestion.registro_medico_id_registro_seq OWNED BY gestion.registro_medico.id_registro;

ALTER TABLE gestion.personas ADD CONSTRAINT estados_civiles_personas_fk
FOREIGN KEY (id_estado_civil)
REFERENCES gestion.estados_civiles (id_estado_civil)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.personas ADD CONSTRAINT ubicaciones_personas_fk
FOREIGN KEY (id_ubicacion)
REFERENCES gestion.ubicaciones (id_ubicacion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.anotaciones ADD CONSTRAINT tipos_anotacion_anotaciones_fk
FOREIGN KEY (id_tipo_anotacion)
REFERENCES gestion.tipos_anotaciones (id_tipo_anotacion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.roles_permisos ADD CONSTRAINT roles_roles_permisos_fk
FOREIGN KEY (id_roles)
REFERENCES gestion.roles (id_roles)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.roles_personas ADD CONSTRAINT roles_roles_personas_fk
FOREIGN KEY (id_roles)
REFERENCES gestion.roles (id_roles)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.roles_permisos ADD CONSTRAINT permisos_roles_permisos_fk
FOREIGN KEY (id_permiso)
REFERENCES gestion.permisos (id_permiso)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.items ADD CONSTRAINT categorias_items_fk
FOREIGN KEY (id_categoria)
REFERENCES gestion.categorias (id_categoria)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.internados ADD CONSTRAINT salas_internados_fk
FOREIGN KEY (id_sala)
REFERENCES gestion.salas (id_sala)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.depositos_articulos ADD CONSTRAINT depositos_depositos_articulos_fk
FOREIGN KEY (id_deposito)
REFERENCES gestion.depositos (id_deposito)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.articulos ADD CONSTRAINT tipos_articulo_articulos_fk
FOREIGN KEY (id_tipo_articulo)
REFERENCES gestion.tipos_articulo (id_tipo_articulo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.depositos_articulos ADD CONSTRAINT articulos_depositos_articulos_fk
FOREIGN KEY (id_articulo)
REFERENCES gestion.articulos (id_articulo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.medicamento_internado ADD CONSTRAINT articulos_medicamento_internado_fk
FOREIGN KEY (id_medicamento)
REFERENCES gestion.articulos (id_articulo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.desechables ADD CONSTRAINT articulos_desechables_fk
FOREIGN KEY (id_articulo)
REFERENCES gestion.articulos (id_articulo)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.medicamento_internado ADD CONSTRAINT unidades_medicamento_internado_fk
FOREIGN KEY (id_unidad_medida)
REFERENCES gestion.unidades (id_unidad_medida)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.items ADD CONSTRAINT unidades_items_fk
FOREIGN KEY (id_unidad_medida)
REFERENCES gestion.unidades (id_unidad_medida)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.registro_medico ADD CONSTRAINT items_registro_medico_fk
FOREIGN KEY (id_item)
REFERENCES gestion.items (id_item)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.turno_enfermero ADD CONSTRAINT turnos_turno_enfermero_fk
FOREIGN KEY (id_turno)
REFERENCES gestion.turnos (id_turno)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.internados ADD CONSTRAINT tipo_internacion_internados_fk
FOREIGN KEY (id_tipo_internacion)
REFERENCES gestion.tipo_internacion (id_tipo_internacion)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.internados ADD CONSTRAINT tipo_clientes_internados_fk
FOREIGN KEY (id_tipo_cliente)
REFERENCES gestion.tipos_clientes (id_tipo_cliente)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.internados ADD CONSTRAINT personas_internados_fk
FOREIGN KEY (id_persona)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.roles_personas ADD CONSTRAINT personas_roles_personas_fk
FOREIGN KEY (id_persona)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.medicos_asignados ADD CONSTRAINT personas_medicos_asignados_fk
FOREIGN KEY (id_medico)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.registro_medico ADD CONSTRAINT personas_registro_medico_fk
FOREIGN KEY (id_registrador)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.turno_enfermero ADD CONSTRAINT personas_turno_enfermero_fk
FOREIGN KEY (id_enfermero)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.desechables ADD CONSTRAINT personas_desechables_fk
FOREIGN KEY (id_enfermero)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.anotaciones ADD CONSTRAINT personas_notas_fk
FOREIGN KEY (id_registrador)
REFERENCES gestion.personas (id_persona)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.desechables ADD CONSTRAINT turno_enfermero_desechables_fk
FOREIGN KEY (id_turno_enfermero)
REFERENCES gestion.turno_enfermero (id_turno_enfermero)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.registro_medico ADD CONSTRAINT turno_enfermero_historial_medico_fk
FOREIGN KEY (id_turno_enfermero)
REFERENCES gestion.turno_enfermero (id_turno_enfermero)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.registro_medico ADD CONSTRAINT internados_historial_medico_fk
FOREIGN KEY (id_internado)
REFERENCES gestion.internados (id_internado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.medicamento_internado ADD CONSTRAINT internados_medicamento_internado_fk
FOREIGN KEY (id_internado)
REFERENCES gestion.internados (id_internado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.medicos_asignados ADD CONSTRAINT internados_medicos_asignados_fk
FOREIGN KEY (id_internado)
REFERENCES gestion.internados (id_internado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.desechables ADD CONSTRAINT internados_desechables_fk
FOREIGN KEY (id_internado)
REFERENCES gestion.internados (id_internado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.anotaciones ADD CONSTRAINT internados_notas_fk
FOREIGN KEY (id_internado)
REFERENCES gestion.internados (id_internado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.medicamento_suministrado ADD CONSTRAINT medicamento_internado_medicamento_suministrado_fk
FOREIGN KEY (id_medicamento_internado)
REFERENCES gestion.medicamento_internado (id_medicamento_internado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE gestion.registro_medico ADD CONSTRAINT medicamento_suministrado_registro_medico_fk
FOREIGN KEY (id_medicamento_suministrado)
REFERENCES gestion.medicamento_suministrado (id_medicamento_suministrado)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;
