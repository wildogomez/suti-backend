CREATE USER suti_admin 
WITH ENCRYPTED PASSWORD 'password';        

-- drop user suti_admin;

CREATE DATABASE suti
WITH ENCODING = 'UTF8';

GRANT ALL PRIVILEGES ON DATABASE suti TO suti_admin;