package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.AnotacionesBean;
import py.edu.fpuna.suti.bean.SalasBean;
import py.edu.fpuna.suti.model.Anotaciones;
import py.edu.fpuna.suti.model.Salas;
import py.edu.fpuna.suti.service.AnotacionesService;
import py.edu.fpuna.suti.service.SalasService;

@RestController
public class SalasController {

	private static Logger log = LoggerFactory.getLogger(SalasController.class);
	
	@Autowired
	private SalasService salasService;
	
	
	@GetMapping("/sala")
	public ResponseEntity<List<SalasBean>> getSalas(
			@RequestParam(name = "descripcion", required = false) String descripcion) {
		log.info("Entra la peticion a GET /salas");
		List<Salas> salasModel = null;
		List<SalasBean> salasBeans = new ArrayList<SalasBean>();

		try {
			salasModel = salasService.getSalas();

			for (Salas salaModel : salasModel) {
				SalasBean salaBean = new SalasBean();
				salaBean.setId_sala(salaModel.getIdSala());
				salaBean.setDescripcion(salaModel.getDescripcion());
				salaBean.setNumero_sala(salaModel.getNumeroSala());
				salaBean.setHabilitado(salaModel.getHabilitado());
				salasBeans.add(salaBean);
			}

			return new ResponseEntity<List<SalasBean>>(salasBeans, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<SalasBean>>(salasBeans, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping("/sala")
	public ResponseEntity<Salas> postSala(@RequestBody(required = true) Salas newSala) {
		log.info("Entra la peticion a POST /sala");
		Salas sala = null;
		try {
			sala = salasService.postSalas(newSala);
			return new ResponseEntity<Salas>(sala, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Salas>(sala, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
