package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.DepositosBean;
import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.service.DepositosService;

@RestController
public class DepositosController {
	
	private static Logger log = LoggerFactory.getLogger(DepositosController.class);
	
	@Autowired
	private DepositosService depositosService;
	
	
	@GetMapping("/deposito")
	public ResponseEntity<List<DepositosBean>> getDepositos(@RequestParam(name="descripcion", required=false) String descripcion) {
		log.info("Entra la peticion a GET /deposito");
		List<Depositos> depositosModel = null;
		List<DepositosBean> depositosBean = new ArrayList<DepositosBean>();
		
		try {
			depositosModel = depositosService.getDepositos();
			
			for (Depositos depositoModel : depositosModel) {
				DepositosBean depositoBean = new DepositosBean();
				depositoBean.setIdDeposito(depositoModel.getIdDeposito());
				depositoBean.setCodigo(depositoModel.getCodigo());
				depositoBean.setDescripcion(depositoModel.getDescripcion());
				depositosBean.add(depositoBean);
			}
			
			return new ResponseEntity<List<DepositosBean>>(depositosBean, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<DepositosBean>>(depositosBean, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@PostMapping("/deposito")
	public ResponseEntity<Depositos> postDeposito(@RequestBody(required=true) Depositos newDeposito) {
		log.info("Entra la peticion a POST /deposito");
		Depositos deposito = null;
		try {
			deposito = depositosService.postDeposito(newDeposito);
			return new ResponseEntity<Depositos>(deposito, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Depositos>(deposito, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
