package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.DepositosBean;
import py.edu.fpuna.suti.bean.TurnosBean;
import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.model.Turnos;
import py.edu.fpuna.suti.service.DepositosService;
import py.edu.fpuna.suti.service.TurnosService;

@RestController
public class TurnosController {

	private static Logger log = LoggerFactory.getLogger(TurnosController.class);

	@Autowired
	private TurnosService turnosService;

	@GetMapping("/turno")
	public ResponseEntity<List<TurnosBean>> getTurnos(
			@RequestParam(name = "descripcion", required = false) String descripcion) {
		log.info("Entra la peticion a GET /turno");
		List<Turnos> turnosModel = null;
		List<TurnosBean> turnosBeans = new ArrayList<TurnosBean>();

		try {
			turnosModel = turnosService.getTurnos();

			for (Turnos turnoModel : turnosModel) {
				TurnosBean turnoBean = new TurnosBean();
				turnoBean.setId_turno(turnoModel.getIdTurno());
				turnoBean.setDia_semana(turnoModel.getDiaSemana());
				turnoBean.setHora_inicio(turnoModel.getHoraInicio());
				turnoBean.setHora_fin(turnoModel.getHoraFin());
				turnosBeans.add(turnoBean);
			}
			return new ResponseEntity<List<TurnosBean>>(turnosBeans, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<TurnosBean>>(turnosBeans, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/turno")
	public ResponseEntity<Turnos> postTurno(@RequestBody(required = true) Turnos newTurno) {
		log.info("Entra la peticion a POST /turno");
		Turnos turno = null;
		try {
			turno = turnosService.postTurno(newTurno);
			return new ResponseEntity<Turnos>(turno, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Turnos>(turno, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
