package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.AnotacionesBean;
import py.edu.fpuna.suti.model.Anotaciones;
import py.edu.fpuna.suti.service.AnotacionesService;

@RestController
public class AnotacionesController {

	private static Logger log = LoggerFactory.getLogger(AnotacionesController.class);
	
	@Autowired
	private AnotacionesService anotacionesService;
	
	@GetMapping("/anotacion")
	public ResponseEntity<List<AnotacionesBean>> getAnotaciones(
			@RequestParam(name = "descripcion", required = false) String descripcion) {
		log.info("Entra la peticion a GET /anotacion");
		List<Anotaciones> anotacionesModel = null;
		List<AnotacionesBean> anotacionesBeans = new ArrayList<AnotacionesBean>();

		try {
			anotacionesModel = anotacionesService.getAnotaciones();

			for (Anotaciones anotacionModel : anotacionesModel) {
				AnotacionesBean anotacionBean = new AnotacionesBean();
				anotacionBean.setId_anotacion(anotacionModel.getIdAnotacion());;
				anotacionBean.setId_registrador(anotacionModel.getIdRegistrador());
				anotacionBean.setId_tipo_anotacion(anotacionModel.getIdTipoAnotacion());
				anotacionesBeans.add(anotacionBean);
			}

			return new ResponseEntity<List<AnotacionesBean>>(anotacionesBeans, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<AnotacionesBean>>(anotacionesBeans, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@PostMapping("/anotacion")
	public ResponseEntity<Anotaciones> postAnotacion(@RequestBody(required = true) Anotaciones newAnotacion) {
		log.info("Entra la peticion a POST /anotacion");
		Anotaciones anotacion = null;
		try {
			anotacion = anotacionesService.postAnotacion(newAnotacion);
			return new ResponseEntity<Anotaciones>(anotacion, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Anotaciones>(anotacion, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
