package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.ArticulosBean;
import py.edu.fpuna.suti.model.Articulos;
import py.edu.fpuna.suti.service.ArticulosService;

@RestController
public class ArticulosController {

	private static Logger log = LoggerFactory.getLogger(ArticulosController.class);

	@Autowired
	private ArticulosService articulosService;

	@GetMapping("/articulo")
	public ResponseEntity<List<ArticulosBean>> getArticulos(
			@RequestParam(name = "descripcion", required = false) String descripcion) {
		log.info("Entra la peticion a GET /articulo");
		List<Articulos> articulosModel = null;
		List<ArticulosBean> articulosBeans = new ArrayList<ArticulosBean>();

		try {
			articulosModel = articulosService.getArticulos();

			for (Articulos articuloModel : articulosModel) {
				ArticulosBean articuloBean = new ArticulosBean();
				articuloBean.setId_articulo(articuloModel.getIdArticulo());
				articuloBean.setCodigo(articuloModel.getCodigo());
				articuloBean.setDescripcion(articuloModel.getDescripcion());
				articuloBean.setId_tipo_articulo(articuloModel.getIdTipoArticulo());
				articulosBeans.add(articuloBean);
			}

			return new ResponseEntity<List<ArticulosBean>>(articulosBeans, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<ArticulosBean>>(articulosBeans, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/articulo")
	public ResponseEntity<Articulos> postAnotacion(@RequestBody(required = true) Articulos newArticulo) {
		log.info("Entra la peticion a POST /articulo");
		Articulos articulo = null;
		try {
			articulo = articulosService.postArticulo(newArticulo);
			return new ResponseEntity<Articulos>(articulo, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Articulos>(articulo, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
