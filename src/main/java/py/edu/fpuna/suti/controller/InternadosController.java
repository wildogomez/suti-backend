package py.edu.fpuna.suti.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.model.Internados;
import py.edu.fpuna.suti.model.RegistroMedico;
import py.edu.fpuna.suti.service.InternadosService;

@RestController
public class InternadosController {

	private static Logger log = LoggerFactory.getLogger(InternadosController.class);
		
	@Autowired
	private InternadosService internadosService;
	
	
	@GetMapping("/internado")
	public ResponseEntity<Internados> getInternado(@RequestParam(name="id", required=true) int id) {
		log.info("Entra la peticion a GET /internado");
		Internados internado= null;
		
		try {
			internado = internadosService.getInternado(id);
			return new ResponseEntity<Internados>(internado, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Internados>(internado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@PostMapping("/internado")
	public ResponseEntity<Internados> postInternado(@RequestBody(required=true) Internados newInternado) {
		log.info("Entra la peticion a POST /internado");
		Internados internado= null;
		try {
			internado = internadosService.postInternados(newInternado);
			return new ResponseEntity<Internados>(internado, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Internados>(internado, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	@GetMapping("/internado/registro")
	public ResponseEntity<List<RegistroMedico>> getInternadoRegistros(
			@RequestParam(name="id_internado", required=true) int id,
			@RequestParam(name="fecha_hora_inicio", required=false) String fechaHoraInicio,
			@RequestParam(name="fecha_hora_fin", required=false) String fechaHoraFin) {
		log.info("Entra la peticion a GET /internado/registro");
		List<RegistroMedico> registros = null;
		
		try {
			registros = internadosService.getInternadoRegistros(id, fechaHoraInicio, fechaHoraFin);
			return new ResponseEntity<List<RegistroMedico>>(registros, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<List<RegistroMedico>>(registros, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
