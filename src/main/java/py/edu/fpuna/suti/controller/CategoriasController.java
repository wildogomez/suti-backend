package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.CategoriasBean;
import py.edu.fpuna.suti.bean.DepositosBean;
import py.edu.fpuna.suti.model.Categorias;
import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.service.CategoriasService;

@RestController
public class CategoriasController {

	private static Logger log = LoggerFactory.getLogger(CategoriasController.class);

	@Autowired
	private CategoriasService categoriasService;

	@GetMapping("/categoria")
	public ResponseEntity<List<CategoriasBean>> getDepositos(
			@RequestParam(name = "descripcion", required = false) String descripcion) {
		log.info("Entra la peticion a GET /categoria");
		List<Categorias> categoriasModel = null;
		List<CategoriasBean> categoriasBean = new ArrayList<CategoriasBean>();

		try {
			categoriasModel = categoriasService.getCategorias();

			for (Categorias categoriaModel : categoriasModel) {
				CategoriasBean categoriaBean = new CategoriasBean();
				categoriaBean.setId_categoria(categoriaModel.getIdCategoria());
				categoriaBean.setDescripcion(categoriaModel.getDescripcion());
				categoriasBean.add(categoriaBean);
			}

			return new ResponseEntity<List<CategoriasBean>>(categoriasBean, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<CategoriasBean>>(categoriasBean, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PostMapping("/categoria")
	public ResponseEntity<Categorias> postCategoria(@RequestBody(required = true) Categorias newCategoria) {
		log.info("Entra la peticion a POST /categoria");
		Categorias categoria = null;
		try {
			categoria = categoriasService.postCategoria(newCategoria);
			return new ResponseEntity<Categorias>(categoria, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<Categorias>(categoria, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

}
