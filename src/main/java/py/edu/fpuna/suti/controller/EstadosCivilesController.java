package py.edu.fpuna.suti.controller;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import py.edu.fpuna.suti.bean.DepositosBean;
import py.edu.fpuna.suti.bean.EstadosCivilesBean;
import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.model.EstadosCiviles;
import py.edu.fpuna.suti.service.DepositosService;
import py.edu.fpuna.suti.service.EstadosCivilesService;

@RestController
public class EstadosCivilesController {
	
	private static Logger log = LoggerFactory.getLogger(EstadosCivilesController.class);
	
	@Autowired
	private EstadosCivilesService estadosCivilesService;
	
	
	@GetMapping("/estadoCivil")
	public ResponseEntity<List<EstadosCivilesBean>> getEstadosCiviles(
			@RequestParam(name="descripcion", required=false) String descripcion) {
		log.info("Entra la peticion a GET /estadoCivil");
		List<EstadosCiviles> estadosCivilesModel = null;
		List<EstadosCivilesBean> estadosCivilesBean = new ArrayList<EstadosCivilesBean>();
		
		try {
			estadosCivilesModel = estadosCivilesService.getEstadosCiviles();
			
			for (EstadosCiviles estadoCivilModel : estadosCivilesModel) {
				EstadosCivilesBean estadoCivilBean = new EstadosCivilesBean();
				estadoCivilBean.setId_estado_civil(estadoCivilModel.getIdEstadoCivil());
				estadoCivilBean.setDescripcion(estadoCivilModel.getDescripcion());
				estadosCivilesBean.add(estadoCivilBean);
			}
			
			return new ResponseEntity<List<EstadosCivilesBean>>(estadosCivilesBean, HttpStatus.OK);
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			return new ResponseEntity<List<EstadosCivilesBean>>(estadosCivilesBean, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	

}
