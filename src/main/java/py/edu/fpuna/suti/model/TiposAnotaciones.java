/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "tipos_anotaciones")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposAnotaciones.findAll", query = "SELECT t FROM TiposAnotaciones t")
    , @NamedQuery(name = "TiposAnotaciones.findByIdTipoAnotacion", query = "SELECT t FROM TiposAnotaciones t WHERE t.idTipoAnotacion = :idTipoAnotacion")
    , @NamedQuery(name = "TiposAnotaciones.findByDescripcion", query = "SELECT t FROM TiposAnotaciones t WHERE t.descripcion = :descripcion")})
public class TiposAnotaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_tipo_anotacion")
    private Integer idTipoAnotacion;
    @Basic(optional = false)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoAnotacion")
    private List<Anotaciones> anotacionesList;

    public TiposAnotaciones() {
    }

    public TiposAnotaciones(Integer idTipoAnotacion) {
        this.idTipoAnotacion = idTipoAnotacion;
    }

    public TiposAnotaciones(Integer idTipoAnotacion, String descripcion) {
        this.idTipoAnotacion = idTipoAnotacion;
        this.descripcion = descripcion;
    }

    public Integer getIdTipoAnotacion() {
        return idTipoAnotacion;
    }

    public void setIdTipoAnotacion(Integer idTipoAnotacion) {
        this.idTipoAnotacion = idTipoAnotacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Anotaciones> getAnotacionesList() {
        return anotacionesList;
    }

    public void setAnotacionesList(List<Anotaciones> anotacionesList) {
        this.anotacionesList = anotacionesList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoAnotacion != null ? idTipoAnotacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposAnotaciones)) {
            return false;
        }
        TiposAnotaciones other = (TiposAnotaciones) object;
        if ((this.idTipoAnotacion == null && other.idTipoAnotacion != null) || (this.idTipoAnotacion != null && !this.idTipoAnotacion.equals(other.idTipoAnotacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.TiposAnotaciones[ idTipoAnotacion=" + idTipoAnotacion + " ]";
    }
    
}
