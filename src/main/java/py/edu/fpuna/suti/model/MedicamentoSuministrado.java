/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "medicamento_suministrado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicamentoSuministrado.findAll", query = "SELECT m FROM MedicamentoSuministrado m")
    , @NamedQuery(name = "MedicamentoSuministrado.findByIdMedicamentoSuministrado", query = "SELECT m FROM MedicamentoSuministrado m WHERE m.idMedicamentoSuministrado = :idMedicamentoSuministrado")
    , @NamedQuery(name = "MedicamentoSuministrado.findByFechaHora", query = "SELECT m FROM MedicamentoSuministrado m WHERE m.fechaHora = :fechaHora")
    , @NamedQuery(name = "MedicamentoSuministrado.findByCantidad", query = "SELECT m FROM MedicamentoSuministrado m WHERE m.cantidad = :cantidad")})
public class MedicamentoSuministrado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_medicamento_suministrado")
    private Integer idMedicamentoSuministrado;
    @Basic(optional = false)
    @Column(name = "fecha_hora")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaHora;
    @Basic(optional = false)
    private float cantidad;
    @OneToMany(mappedBy = "idMedicamentoSuministrado")
    private List<RegistroMedico> registroMedicoList;
    @JoinColumn(name = "id_medicamento_internado", referencedColumnName = "id_medicamento_internado")
    @ManyToOne(optional = false)
    private MedicamentoInternado idMedicamentoInternado;

    public MedicamentoSuministrado() {
    }

    public MedicamentoSuministrado(Integer idMedicamentoSuministrado) {
        this.idMedicamentoSuministrado = idMedicamentoSuministrado;
    }

    public MedicamentoSuministrado(Integer idMedicamentoSuministrado, Date fechaHora, float cantidad) {
        this.idMedicamentoSuministrado = idMedicamentoSuministrado;
        this.fechaHora = fechaHora;
        this.cantidad = cantidad;
    }

    public Integer getIdMedicamentoSuministrado() {
        return idMedicamentoSuministrado;
    }

    public void setIdMedicamentoSuministrado(Integer idMedicamentoSuministrado) {
        this.idMedicamentoSuministrado = idMedicamentoSuministrado;
    }

    public Date getFechaHora() {
        return fechaHora;
    }

    public void setFechaHora(Date fechaHora) {
        this.fechaHora = fechaHora;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    @XmlTransient
    public List<RegistroMedico> getRegistroMedicoList() {
        return registroMedicoList;
    }

    public void setRegistroMedicoList(List<RegistroMedico> registroMedicoList) {
        this.registroMedicoList = registroMedicoList;
    }

    public MedicamentoInternado getIdMedicamentoInternado() {
        return idMedicamentoInternado;
    }

    public void setIdMedicamentoInternado(MedicamentoInternado idMedicamentoInternado) {
        this.idMedicamentoInternado = idMedicamentoInternado;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMedicamentoSuministrado != null ? idMedicamentoSuministrado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicamentoSuministrado)) {
            return false;
        }
        MedicamentoSuministrado other = (MedicamentoSuministrado) object;
        if ((this.idMedicamentoSuministrado == null && other.idMedicamentoSuministrado != null) || (this.idMedicamentoSuministrado != null && !this.idMedicamentoSuministrado.equals(other.idMedicamentoSuministrado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.MedicamentoSuministrado[ idMedicamentoSuministrado=" + idMedicamentoSuministrado + " ]";
    }
    
}
