/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "tipos_articulo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposArticulo.findAll", query = "SELECT t FROM TiposArticulo t")
    , @NamedQuery(name = "TiposArticulo.findByIdTipoArticulo", query = "SELECT t FROM TiposArticulo t WHERE t.idTipoArticulo = :idTipoArticulo")
    , @NamedQuery(name = "TiposArticulo.findByDescripcion", query = "SELECT t FROM TiposArticulo t WHERE t.descripcion = :descripcion")})
public class TiposArticulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_tipo_articulo")
    private Integer idTipoArticulo;
    @Basic(optional = false)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoArticulo")
    private List<Articulos> articulosList;

    public TiposArticulo() {
    }

    public TiposArticulo(Integer idTipoArticulo) {
        this.idTipoArticulo = idTipoArticulo;
    }

    public TiposArticulo(Integer idTipoArticulo, String descripcion) {
        this.idTipoArticulo = idTipoArticulo;
        this.descripcion = descripcion;
    }

    public Integer getIdTipoArticulo() {
        return idTipoArticulo;
    }

    public void setIdTipoArticulo(Integer idTipoArticulo) {
        this.idTipoArticulo = idTipoArticulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Articulos> getArticulosList() {
        return articulosList;
    }

    public void setArticulosList(List<Articulos> articulosList) {
        this.articulosList = articulosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoArticulo != null ? idTipoArticulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposArticulo)) {
            return false;
        }
        TiposArticulo other = (TiposArticulo) object;
        if ((this.idTipoArticulo == null && other.idTipoArticulo != null) || (this.idTipoArticulo != null && !this.idTipoArticulo.equals(other.idTipoArticulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.TiposArticulo[ idTipoArticulo=" + idTipoArticulo + " ]";
    }
    
}
