/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "roles_permisos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolesPermisos.findAll", query = "SELECT r FROM RolesPermisos r")
    , @NamedQuery(name = "RolesPermisos.findByIdRolPermiso", query = "SELECT r FROM RolesPermisos r WHERE r.idRolPermiso = :idRolPermiso")
    , @NamedQuery(name = "RolesPermisos.findByHabilitado", query = "SELECT r FROM RolesPermisos r WHERE r.habilitado = :habilitado")
    , @NamedQuery(name = "RolesPermisos.findByFechaInsercion", query = "SELECT r FROM RolesPermisos r WHERE r.fechaInsercion = :fechaInsercion")
    , @NamedQuery(name = "RolesPermisos.findByFechaActualizacion", query = "SELECT r FROM RolesPermisos r WHERE r.fechaActualizacion = :fechaActualizacion")})
public class RolesPermisos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_rol_permiso")
    private Integer idRolPermiso;
    @Basic(optional = false)
    private boolean habilitado;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_permiso", referencedColumnName = "id_permiso")
    @ManyToOne(optional = false)
    private Permisos idPermiso;
    @JoinColumn(name = "id_roles", referencedColumnName = "id_roles")
    @ManyToOne(optional = false)
    private Roles idRoles;

    public RolesPermisos() {
    }

    public RolesPermisos(Integer idRolPermiso) {
        this.idRolPermiso = idRolPermiso;
    }

    public RolesPermisos(Integer idRolPermiso, boolean habilitado, Date fechaInsercion) {
        this.idRolPermiso = idRolPermiso;
        this.habilitado = habilitado;
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdRolPermiso() {
        return idRolPermiso;
    }

    public void setIdRolPermiso(Integer idRolPermiso) {
        this.idRolPermiso = idRolPermiso;
    }

    public boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Permisos getIdPermiso() {
        return idPermiso;
    }

    public void setIdPermiso(Permisos idPermiso) {
        this.idPermiso = idPermiso;
    }

    public Roles getIdRoles() {
        return idRoles;
    }

    public void setIdRoles(Roles idRoles) {
        this.idRoles = idRoles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRolPermiso != null ? idRolPermiso.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesPermisos)) {
            return false;
        }
        RolesPermisos other = (RolesPermisos) object;
        if ((this.idRolPermiso == null && other.idRolPermiso != null) || (this.idRolPermiso != null && !this.idRolPermiso.equals(other.idRolPermiso))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.RolesPermisos[ idRolPermiso=" + idRolPermiso + " ]";
    }
    
}
