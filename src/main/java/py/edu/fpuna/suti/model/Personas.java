/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Personas.findAll", query = "SELECT p FROM Personas p")
    , @NamedQuery(name = "Personas.findByIdPersona", query = "SELECT p FROM Personas p WHERE p.idPersona = :idPersona")
    , @NamedQuery(name = "Personas.findByIdPaciente", query = "SELECT p FROM Personas p WHERE p.idPaciente = :idPaciente")
    , @NamedQuery(name = "Personas.findByNombre", query = "SELECT p FROM Personas p WHERE p.nombre = :nombre")
    , @NamedQuery(name = "Personas.findByApellido", query = "SELECT p FROM Personas p WHERE p.apellido = :apellido")
    , @NamedQuery(name = "Personas.findByFechaNacimiento", query = "SELECT p FROM Personas p WHERE p.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Personas.findByDireccion", query = "SELECT p FROM Personas p WHERE p.direccion = :direccion")
    , @NamedQuery(name = "Personas.findByCantidadHijos", query = "SELECT p FROM Personas p WHERE p.cantidadHijos = :cantidadHijos")
    , @NamedQuery(name = "Personas.findByObservacion", query = "SELECT p FROM Personas p WHERE p.observacion = :observacion")})
public class Personas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_persona")
    private Integer idPersona;
    @Basic(optional = false)
    @Column(name = "id_paciente")
    private String idPaciente;
    @Basic(optional = false)
    private String nombre;
    @Basic(optional = false)
    private String apellido;
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaNacimiento;
    private String direccion;
    @Basic(optional = false)
    @Column(name = "cantidad_hijos")
    private int cantidadHijos;
    private String observacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEnfermero")
    private List<TurnoEnfermero> turnoEnfermeroList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersona")
    private List<RolesPersonas> rolesPersonasList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idEnfermero")
    private List<Desechables> desechablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRegistrador")
    private List<RegistroMedico> registroMedicoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMedico")
    private List<MedicosAsignados> medicosAsignadosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idRegistrador")
    private List<Anotaciones> anotacionesList;
    @JoinColumn(name = "id_estado_civil", referencedColumnName = "id_estado_civil")
    @ManyToOne
    private EstadosCiviles idEstadoCivil;
    @JoinColumn(name = "id_ubicacion", referencedColumnName = "id_ubicacion")
    @ManyToOne
    private Ubicaciones idUbicacion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idPersona")
    private List<Internados> internadosList;

    public Personas() {
    }

    public Personas(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public Personas(Integer idPersona, String idPaciente, String nombre, String apellido, int cantidadHijos) {
        this.idPersona = idPersona;
        this.idPaciente = idPaciente;
        this.nombre = nombre;
        this.apellido = apellido;
        this.cantidadHijos = cantidadHijos;
    }

    public Integer getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Integer idPersona) {
        this.idPersona = idPersona;
    }

    public String getIdPaciente() {
        return idPaciente;
    }

    public void setIdPaciente(String idPaciente) {
        this.idPaciente = idPaciente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getCantidadHijos() {
        return cantidadHijos;
    }

    public void setCantidadHijos(int cantidadHijos) {
        this.cantidadHijos = cantidadHijos;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    @XmlTransient
    public List<TurnoEnfermero> getTurnoEnfermeroList() {
        return turnoEnfermeroList;
    }

    public void setTurnoEnfermeroList(List<TurnoEnfermero> turnoEnfermeroList) {
        this.turnoEnfermeroList = turnoEnfermeroList;
    }

    @XmlTransient
    public List<RolesPersonas> getRolesPersonasList() {
        return rolesPersonasList;
    }

    public void setRolesPersonasList(List<RolesPersonas> rolesPersonasList) {
        this.rolesPersonasList = rolesPersonasList;
    }

    @XmlTransient
    public List<Desechables> getDesechablesList() {
        return desechablesList;
    }

    public void setDesechablesList(List<Desechables> desechablesList) {
        this.desechablesList = desechablesList;
    }

    @XmlTransient
    public List<RegistroMedico> getRegistroMedicoList() {
        return registroMedicoList;
    }

    public void setRegistroMedicoList(List<RegistroMedico> registroMedicoList) {
        this.registroMedicoList = registroMedicoList;
    }

    @XmlTransient
    public List<MedicosAsignados> getMedicosAsignadosList() {
        return medicosAsignadosList;
    }

    public void setMedicosAsignadosList(List<MedicosAsignados> medicosAsignadosList) {
        this.medicosAsignadosList = medicosAsignadosList;
    }

    @XmlTransient
    public List<Anotaciones> getAnotacionesList() {
        return anotacionesList;
    }

    public void setAnotacionesList(List<Anotaciones> anotacionesList) {
        this.anotacionesList = anotacionesList;
    }

    public EstadosCiviles getIdEstadoCivil() {
        return idEstadoCivil;
    }

    public void setIdEstadoCivil(EstadosCiviles idEstadoCivil) {
        this.idEstadoCivil = idEstadoCivil;
    }

    public Ubicaciones getIdUbicacion() {
        return idUbicacion;
    }

    public void setIdUbicacion(Ubicaciones idUbicacion) {
        this.idUbicacion = idUbicacion;
    }

    @XmlTransient
    public List<Internados> getInternadosList() {
        return internadosList;
    }

    public void setInternadosList(List<Internados> internadosList) {
        this.internadosList = internadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPersona != null ? idPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Personas)) {
            return false;
        }
        Personas other = (Personas) object;
        if ((this.idPersona == null && other.idPersona != null) || (this.idPersona != null && !this.idPersona.equals(other.idPersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Personas[ idPersona=" + idPersona + " ]";
    }
    
}
