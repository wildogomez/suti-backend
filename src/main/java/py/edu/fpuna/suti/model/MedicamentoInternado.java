/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "medicamento_internado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicamentoInternado.findAll", query = "SELECT m FROM MedicamentoInternado m")
    , @NamedQuery(name = "MedicamentoInternado.findByIdMedicamentoInternado", query = "SELECT m FROM MedicamentoInternado m WHERE m.idMedicamentoInternado = :idMedicamentoInternado")
    , @NamedQuery(name = "MedicamentoInternado.findByCantidadAsignada", query = "SELECT m FROM MedicamentoInternado m WHERE m.cantidadAsignada = :cantidadAsignada")
    , @NamedQuery(name = "MedicamentoInternado.findByCantidadUtilizada", query = "SELECT m FROM MedicamentoInternado m WHERE m.cantidadUtilizada = :cantidadUtilizada")
    , @NamedQuery(name = "MedicamentoInternado.findByFechaAsignacion", query = "SELECT m FROM MedicamentoInternado m WHERE m.fechaAsignacion = :fechaAsignacion")})
public class MedicamentoInternado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_medicamento_internado")
    private Integer idMedicamentoInternado;
    @Basic(optional = false)
    @Column(name = "cantidad_asignada")
    private float cantidadAsignada;
    @Basic(optional = false)
    @Column(name = "cantidad_utilizada")
    private float cantidadUtilizada;
    @Basic(optional = false)
    @Column(name = "fecha_asignacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsignacion;
    @JoinColumn(name = "id_medicamento", referencedColumnName = "id_articulo")
    @ManyToOne(optional = false)
    private Articulos idMedicamento;
    @JoinColumn(name = "id_internado", referencedColumnName = "id_internado")
    @ManyToOne(optional = false)
    private Internados idInternado;
    @JoinColumn(name = "id_unidad_medida", referencedColumnName = "id_unidad_medida")
    @ManyToOne(optional = false)
    private Unidades idUnidadMedida;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMedicamentoInternado")
    private List<MedicamentoSuministrado> medicamentoSuministradoList;

    public MedicamentoInternado() {
    }

    public MedicamentoInternado(Integer idMedicamentoInternado) {
        this.idMedicamentoInternado = idMedicamentoInternado;
    }

    public MedicamentoInternado(Integer idMedicamentoInternado, float cantidadAsignada, float cantidadUtilizada, Date fechaAsignacion) {
        this.idMedicamentoInternado = idMedicamentoInternado;
        this.cantidadAsignada = cantidadAsignada;
        this.cantidadUtilizada = cantidadUtilizada;
        this.fechaAsignacion = fechaAsignacion;
    }

    public Integer getIdMedicamentoInternado() {
        return idMedicamentoInternado;
    }

    public void setIdMedicamentoInternado(Integer idMedicamentoInternado) {
        this.idMedicamentoInternado = idMedicamentoInternado;
    }

    public float getCantidadAsignada() {
        return cantidadAsignada;
    }

    public void setCantidadAsignada(float cantidadAsignada) {
        this.cantidadAsignada = cantidadAsignada;
    }

    public float getCantidadUtilizada() {
        return cantidadUtilizada;
    }

    public void setCantidadUtilizada(float cantidadUtilizada) {
        this.cantidadUtilizada = cantidadUtilizada;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public Articulos getIdMedicamento() {
        return idMedicamento;
    }

    public void setIdMedicamento(Articulos idMedicamento) {
        this.idMedicamento = idMedicamento;
    }

    public Internados getIdInternado() {
        return idInternado;
    }

    public void setIdInternado(Internados idInternado) {
        this.idInternado = idInternado;
    }

    public Unidades getIdUnidadMedida() {
        return idUnidadMedida;
    }

    public void setIdUnidadMedida(Unidades idUnidadMedida) {
        this.idUnidadMedida = idUnidadMedida;
    }

    @XmlTransient
    public List<MedicamentoSuministrado> getMedicamentoSuministradoList() {
        return medicamentoSuministradoList;
    }

    public void setMedicamentoSuministradoList(List<MedicamentoSuministrado> medicamentoSuministradoList) {
        this.medicamentoSuministradoList = medicamentoSuministradoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMedicamentoInternado != null ? idMedicamentoInternado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicamentoInternado)) {
            return false;
        }
        MedicamentoInternado other = (MedicamentoInternado) object;
        if ((this.idMedicamentoInternado == null && other.idMedicamentoInternado != null) || (this.idMedicamentoInternado != null && !this.idMedicamentoInternado.equals(other.idMedicamentoInternado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.MedicamentoInternado[ idMedicamentoInternado=" + idMedicamentoInternado + " ]";
    }
    
}
