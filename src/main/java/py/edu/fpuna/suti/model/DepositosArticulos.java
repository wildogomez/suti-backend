/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "depositos_articulos")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DepositosArticulos.findAll", query = "SELECT d FROM DepositosArticulos d")
    , @NamedQuery(name = "DepositosArticulos.findByIdDepositoArticulo", query = "SELECT d FROM DepositosArticulos d WHERE d.idDepositoArticulo = :idDepositoArticulo")
    , @NamedQuery(name = "DepositosArticulos.findByFechaVencimiento", query = "SELECT d FROM DepositosArticulos d WHERE d.fechaVencimiento = :fechaVencimiento")
    , @NamedQuery(name = "DepositosArticulos.findByCantidad", query = "SELECT d FROM DepositosArticulos d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "DepositosArticulos.findByFechaInsercion", query = "SELECT d FROM DepositosArticulos d WHERE d.fechaInsercion = :fechaInsercion")
    , @NamedQuery(name = "DepositosArticulos.findByFechaActualizacion", query = "SELECT d FROM DepositosArticulos d WHERE d.fechaActualizacion = :fechaActualizacion")})
public class DepositosArticulos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_deposito_articulo")
    private Integer idDepositoArticulo;
    @Basic(optional = false)
    @Column(name = "fecha_vencimiento")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaVencimiento;
    @Basic(optional = false)
    private int cantidad;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_articulo", referencedColumnName = "id_articulo")
    @ManyToOne(optional = false)
    private Articulos idArticulo;
    @JoinColumn(name = "id_deposito", referencedColumnName = "id_deposito")
    @ManyToOne(optional = false)
    private Depositos idDeposito;

    public DepositosArticulos() {
    }

    public DepositosArticulos(Integer idDepositoArticulo) {
        this.idDepositoArticulo = idDepositoArticulo;
    }

    public DepositosArticulos(Integer idDepositoArticulo, Date fechaVencimiento, int cantidad, Date fechaInsercion) {
        this.idDepositoArticulo = idDepositoArticulo;
        this.fechaVencimiento = fechaVencimiento;
        this.cantidad = cantidad;
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdDepositoArticulo() {
        return idDepositoArticulo;
    }

    public void setIdDepositoArticulo(Integer idDepositoArticulo) {
        this.idDepositoArticulo = idDepositoArticulo;
    }

    public Date getFechaVencimiento() {
        return fechaVencimiento;
    }

    public void setFechaVencimiento(Date fechaVencimiento) {
        this.fechaVencimiento = fechaVencimiento;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Articulos getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Articulos idArticulo) {
        this.idArticulo = idArticulo;
    }

    public Depositos getIdDeposito() {
        return idDeposito;
    }

    public void setIdDeposito(Depositos idDeposito) {
        this.idDeposito = idDeposito;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDepositoArticulo != null ? idDepositoArticulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DepositosArticulos)) {
            return false;
        }
        DepositosArticulos other = (DepositosArticulos) object;
        if ((this.idDepositoArticulo == null && other.idDepositoArticulo != null) || (this.idDepositoArticulo != null && !this.idDepositoArticulo.equals(other.idDepositoArticulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.DepositosArticulos[ idDepositoArticulo=" + idDepositoArticulo + " ]";
    }
    
}
