/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Salas.findAll", query = "SELECT s FROM Salas s")
    , @NamedQuery(name = "Salas.findByIdSala", query = "SELECT s FROM Salas s WHERE s.idSala = :idSala")
    , @NamedQuery(name = "Salas.findByDescripcion", query = "SELECT s FROM Salas s WHERE s.descripcion = :descripcion")
    , @NamedQuery(name = "Salas.findByNumeroSala", query = "SELECT s FROM Salas s WHERE s.numeroSala = :numeroSala")
    , @NamedQuery(name = "Salas.findByHabilitado", query = "SELECT s FROM Salas s WHERE s.habilitado = :habilitado")})
public class Salas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_sala")
    private Integer idSala;
    private String descripcion;
    @Basic(optional = false)
    @Column(name = "numero_sala")
    private String numeroSala;
    @Basic(optional = false)
    private boolean habilitado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idSala")
    private List<Internados> internadosList;

    public Salas() {
    }

    public Salas(Integer idSala) {
        this.idSala = idSala;
    }

    public Salas(Integer idSala, String numeroSala, boolean habilitado) {
        this.idSala = idSala;
        this.numeroSala = numeroSala;
        this.habilitado = habilitado;
    }

    public Integer getIdSala() {
        return idSala;
    }

    public void setIdSala(Integer idSala) {
        this.idSala = idSala;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNumeroSala() {
        return numeroSala;
    }

    public void setNumeroSala(String numeroSala) {
        this.numeroSala = numeroSala;
    }

    public boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    @XmlTransient
    public List<Internados> getInternadosList() {
        return internadosList;
    }

    public void setInternadosList(List<Internados> internadosList) {
        this.internadosList = internadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idSala != null ? idSala.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Salas)) {
            return false;
        }
        Salas other = (Salas) object;
        if ((this.idSala == null && other.idSala != null) || (this.idSala != null && !this.idSala.equals(other.idSala))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Salas[ idSala=" + idSala + " ]";
    }
    
}
