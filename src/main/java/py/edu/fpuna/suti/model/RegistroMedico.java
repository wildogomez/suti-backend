/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "registro_medico")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegistroMedico.findAll", query = "SELECT r FROM RegistroMedico r")
    , @NamedQuery(name = "RegistroMedico.findByIdRegistro", query = "SELECT r FROM RegistroMedico r WHERE r.idRegistro = :idRegistro")
    , @NamedQuery(name = "RegistroMedico.findByValorRegistro", query = "SELECT r FROM RegistroMedico r WHERE r.valorRegistro = :valorRegistro")
    , @NamedQuery(name = "RegistroMedico.findByFechaInsercion", query = "SELECT r FROM RegistroMedico r WHERE r.fechaInsercion = :fechaInsercion")
    , @NamedQuery(name = "RegistroMedico.findByFechaActualizacion", query = "SELECT r FROM RegistroMedico r WHERE r.fechaActualizacion = :fechaActualizacion")})
public class RegistroMedico implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro")
    private Integer idRegistro;
    @Basic(optional = false)
    @Column(name = "valor_registro")
    private String valorRegistro;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_internado", referencedColumnName = "id_internado")
    @ManyToOne(optional = false)
    private Internados idInternado;
    @JoinColumn(name = "id_item", referencedColumnName = "id_item")
    @ManyToOne(optional = false)
    private Items idItem;
    @JoinColumn(name = "id_medicamento_suministrado", referencedColumnName = "id_medicamento_suministrado")
    @ManyToOne
    private MedicamentoSuministrado idMedicamentoSuministrado;
    @JoinColumn(name = "id_registrador", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idRegistrador;
    @JoinColumn(name = "id_turno_enfermero", referencedColumnName = "id_turno_enfermero")
    @ManyToOne(optional = false)
    private TurnoEnfermero idTurnoEnfermero;

    public RegistroMedico() {
    }

    public RegistroMedico(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public RegistroMedico(Integer idRegistro, String valorRegistro, Date fechaInsercion, Date fechaActualizacion) {
        this.idRegistro = idRegistro;
        this.valorRegistro = valorRegistro;
        this.fechaInsercion = fechaInsercion;
        this.fechaActualizacion = fechaActualizacion;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public String getValorRegistro() {
        return valorRegistro;
    }

    public void setValorRegistro(String valorRegistro) {
        this.valorRegistro = valorRegistro;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Internados getIdInternado() {
        return idInternado;
    }

    public void setIdInternado(Internados idInternado) {
        this.idInternado = idInternado;
    }

    public Items getIdItem() {
        return idItem;
    }

    public void setIdItem(Items idItem) {
        this.idItem = idItem;
    }

    public MedicamentoSuministrado getIdMedicamentoSuministrado() {
        return idMedicamentoSuministrado;
    }

    public void setIdMedicamentoSuministrado(MedicamentoSuministrado idMedicamentoSuministrado) {
        this.idMedicamentoSuministrado = idMedicamentoSuministrado;
    }

    public Personas getIdRegistrador() {
        return idRegistrador;
    }

    public void setIdRegistrador(Personas idRegistrador) {
        this.idRegistrador = idRegistrador;
    }

    public TurnoEnfermero getIdTurnoEnfermero() {
        return idTurnoEnfermero;
    }

    public void setIdTurnoEnfermero(TurnoEnfermero idTurnoEnfermero) {
        this.idTurnoEnfermero = idTurnoEnfermero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroMedico)) {
            return false;
        }
        RegistroMedico other = (RegistroMedico) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.RegistroMedico[ idRegistro=" + idRegistro + " ]";
    }
    
}
