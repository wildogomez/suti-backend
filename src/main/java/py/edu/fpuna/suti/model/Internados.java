/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Internados.findAll", query = "SELECT i FROM Internados i")
    , @NamedQuery(name = "Internados.findByIdInternado", query = "SELECT i FROM Internados i WHERE i.idInternado = :idInternado")
    , @NamedQuery(name = "Internados.findByFechaInicio", query = "SELECT i FROM Internados i WHERE i.fechaInicio = :fechaInicio")
    , @NamedQuery(name = "Internados.findByFechaFin", query = "SELECT i FROM Internados i WHERE i.fechaFin = :fechaFin")
    , @NamedQuery(name = "Internados.findByFechaFinProbable", query = "SELECT i FROM Internados i WHERE i.fechaFinProbable = :fechaFinProbable")
    , @NamedQuery(name = "Internados.findByNroExpediente", query = "SELECT i FROM Internados i WHERE i.nroExpediente = :nroExpediente")
    , @NamedQuery(name = "Internados.findByIdSucursal", query = "SELECT i FROM Internados i WHERE i.idSucursal = :idSucursal")
    , @NamedQuery(name = "Internados.findByReingreso", query = "SELECT i FROM Internados i WHERE i.reingreso = :reingreso")
    , @NamedQuery(name = "Internados.findBySeguroMedico", query = "SELECT i FROM Internados i WHERE i.seguroMedico = :seguroMedico")})
public class Internados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_internado")
    private Integer idInternado;
    @Basic(optional = false)
    @Column(name = "fecha_inicio")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInicio;
    @Column(name = "fecha_fin")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFin;
    @Basic(optional = false)
    @Column(name = "fecha_fin_probable")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaFinProbable;
    @Basic(optional = false)
    @Column(name = "nro_expediente")
    private BigInteger nroExpediente;
    @Basic(optional = false)
    @Column(name = "id_sucursal")
    private int idSucursal;
    @Basic(optional = false)
    private boolean reingreso;
    @Column(name = "seguro_medico")
    private String seguroMedico;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInternado")
    private List<Desechables> desechablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInternado")
    private List<RegistroMedico> registroMedicoList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInternado")
    private List<MedicosAsignados> medicosAsignadosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInternado")
    private List<Anotaciones> anotacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idInternado")
    private List<MedicamentoInternado> medicamentoInternadoList;
    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idPersona;
    @JoinColumn(name = "id_sala", referencedColumnName = "id_sala")
    @ManyToOne(optional = false)
    private Salas idSala;
    @JoinColumn(name = "id_tipo_internacion", referencedColumnName = "id_tipo_internacion")
    @ManyToOne(optional = false)
    private TipoInternacion idTipoInternacion;
    @JoinColumn(name = "id_tipo_cliente", referencedColumnName = "id_tipo_cliente")
    @ManyToOne(optional = false)
    private TiposClientes idTipoCliente;

    public Internados() {
    }

    public Internados(Integer idInternado) {
        this.idInternado = idInternado;
    }

    public Internados(Integer idInternado, Date fechaInicio, Date fechaFinProbable, BigInteger nroExpediente, int idSucursal, boolean reingreso) {
        this.idInternado = idInternado;
        this.fechaInicio = fechaInicio;
        this.fechaFinProbable = fechaFinProbable;
        this.nroExpediente = nroExpediente;
        this.idSucursal = idSucursal;
        this.reingreso = reingreso;
    }

    public Integer getIdInternado() {
        return idInternado;
    }

    public void setIdInternado(Integer idInternado) {
        this.idInternado = idInternado;
    }

    public Date getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(Date fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public Date getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(Date fechaFin) {
        this.fechaFin = fechaFin;
    }

    public Date getFechaFinProbable() {
        return fechaFinProbable;
    }

    public void setFechaFinProbable(Date fechaFinProbable) {
        this.fechaFinProbable = fechaFinProbable;
    }

    public BigInteger getNroExpediente() {
        return nroExpediente;
    }

    public void setNroExpediente(BigInteger nroExpediente) {
        this.nroExpediente = nroExpediente;
    }

    public int getIdSucursal() {
        return idSucursal;
    }

    public void setIdSucursal(int idSucursal) {
        this.idSucursal = idSucursal;
    }

    public boolean getReingreso() {
        return reingreso;
    }

    public void setReingreso(boolean reingreso) {
        this.reingreso = reingreso;
    }

    public String getSeguroMedico() {
        return seguroMedico;
    }

    public void setSeguroMedico(String seguroMedico) {
        this.seguroMedico = seguroMedico;
    }

    @XmlTransient
    public List<Desechables> getDesechablesList() {
        return desechablesList;
    }

    public void setDesechablesList(List<Desechables> desechablesList) {
        this.desechablesList = desechablesList;
    }

    @XmlTransient
    public List<RegistroMedico> getRegistroMedicoList() {
        return registroMedicoList;
    }

    public void setRegistroMedicoList(List<RegistroMedico> registroMedicoList) {
        this.registroMedicoList = registroMedicoList;
    }

    @XmlTransient
    public List<MedicosAsignados> getMedicosAsignadosList() {
        return medicosAsignadosList;
    }

    public void setMedicosAsignadosList(List<MedicosAsignados> medicosAsignadosList) {
        this.medicosAsignadosList = medicosAsignadosList;
    }

    @XmlTransient
    public List<Anotaciones> getAnotacionesList() {
        return anotacionesList;
    }

    public void setAnotacionesList(List<Anotaciones> anotacionesList) {
        this.anotacionesList = anotacionesList;
    }

    @XmlTransient
    public List<MedicamentoInternado> getMedicamentoInternadoList() {
        return medicamentoInternadoList;
    }

    public void setMedicamentoInternadoList(List<MedicamentoInternado> medicamentoInternadoList) {
        this.medicamentoInternadoList = medicamentoInternadoList;
    }

    public Personas getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Personas idPersona) {
        this.idPersona = idPersona;
    }

    public Salas getIdSala() {
        return idSala;
    }

    public void setIdSala(Salas idSala) {
        this.idSala = idSala;
    }

    public TipoInternacion getIdTipoInternacion() {
        return idTipoInternacion;
    }

    public void setIdTipoInternacion(TipoInternacion idTipoInternacion) {
        this.idTipoInternacion = idTipoInternacion;
    }

    public TiposClientes getIdTipoCliente() {
        return idTipoCliente;
    }

    public void setIdTipoCliente(TiposClientes idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idInternado != null ? idInternado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Internados)) {
            return false;
        }
        Internados other = (Internados) object;
        if ((this.idInternado == null && other.idInternado != null) || (this.idInternado != null && !this.idInternado.equals(other.idInternado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Internados[ idInternado=" + idInternado + " ]";
    }
    
}
