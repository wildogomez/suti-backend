/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "roles_personas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RolesPersonas.findAll", query = "SELECT r FROM RolesPersonas r")
    , @NamedQuery(name = "RolesPersonas.findByIdRolPersona", query = "SELECT r FROM RolesPersonas r WHERE r.idRolPersona = :idRolPersona")
    , @NamedQuery(name = "RolesPersonas.findByHabilitado", query = "SELECT r FROM RolesPersonas r WHERE r.habilitado = :habilitado")
    , @NamedQuery(name = "RolesPersonas.findByFechaInsercion", query = "SELECT r FROM RolesPersonas r WHERE r.fechaInsercion = :fechaInsercion")
    , @NamedQuery(name = "RolesPersonas.findByFechaActualizacion", query = "SELECT r FROM RolesPersonas r WHERE r.fechaActualizacion = :fechaActualizacion")})
public class RolesPersonas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_rol_persona")
    private Integer idRolPersona;
    @Basic(optional = false)
    private boolean habilitado;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_persona", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idPersona;
    @JoinColumn(name = "id_roles", referencedColumnName = "id_roles")
    @ManyToOne(optional = false)
    private Roles idRoles;

    public RolesPersonas() {
    }

    public RolesPersonas(Integer idRolPersona) {
        this.idRolPersona = idRolPersona;
    }

    public RolesPersonas(Integer idRolPersona, boolean habilitado, Date fechaInsercion) {
        this.idRolPersona = idRolPersona;
        this.habilitado = habilitado;
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdRolPersona() {
        return idRolPersona;
    }

    public void setIdRolPersona(Integer idRolPersona) {
        this.idRolPersona = idRolPersona;
    }

    public boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Personas getIdPersona() {
        return idPersona;
    }

    public void setIdPersona(Personas idPersona) {
        this.idPersona = idPersona;
    }

    public Roles getIdRoles() {
        return idRoles;
    }

    public void setIdRoles(Roles idRoles) {
        this.idRoles = idRoles;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRolPersona != null ? idRolPersona.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RolesPersonas)) {
            return false;
        }
        RolesPersonas other = (RolesPersonas) object;
        if ((this.idRolPersona == null && other.idRolPersona != null) || (this.idRolPersona != null && !this.idRolPersona.equals(other.idRolPersona))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.RolesPersonas[ idRolPersona=" + idRolPersona + " ]";
    }
    
}
