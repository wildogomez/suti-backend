/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Depositos.findAll", query = "SELECT d FROM Depositos d")
    , @NamedQuery(name = "Depositos.findByIdDeposito", query = "SELECT d FROM Depositos d WHERE d.idDeposito = :idDeposito")
    , @NamedQuery(name = "Depositos.findByCodigo", query = "SELECT d FROM Depositos d WHERE d.codigo = :codigo")
    , @NamedQuery(name = "Depositos.findByDescripcion", query = "SELECT d FROM Depositos d WHERE d.descripcion = :descripcion")})
public class Depositos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_deposito")
    private Integer idDeposito;
    @Basic(optional = false)
    private String codigo;
    @Basic(optional = false)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idDeposito")
    private List<DepositosArticulos> depositosArticulosList;

    public Depositos() {
    }

    public Depositos(Integer idDeposito) {
        this.idDeposito = idDeposito;
    }

    public Depositos(Integer idDeposito, String codigo, String descripcion) {
        this.idDeposito = idDeposito;
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public Integer getIdDeposito() {
        return idDeposito;
    }

    public void setIdDeposito(Integer idDeposito) {
        this.idDeposito = idDeposito;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<DepositosArticulos> getDepositosArticulosList() {
        return depositosArticulosList;
    }

    public void setDepositosArticulosList(List<DepositosArticulos> depositosArticulosList) {
        this.depositosArticulosList = depositosArticulosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDeposito != null ? idDeposito.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Depositos)) {
            return false;
        }
        Depositos other = (Depositos) object;
        if ((this.idDeposito == null && other.idDeposito != null) || (this.idDeposito != null && !this.idDeposito.equals(other.idDeposito))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Depositos[ idDeposito=" + idDeposito + " ]";
    }
    
}
