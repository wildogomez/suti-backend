/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "tipo_internacion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TipoInternacion.findAll", query = "SELECT t FROM TipoInternacion t")
    , @NamedQuery(name = "TipoInternacion.findByIdTipoInternacion", query = "SELECT t FROM TipoInternacion t WHERE t.idTipoInternacion = :idTipoInternacion")
    , @NamedQuery(name = "TipoInternacion.findByDescripcion", query = "SELECT t FROM TipoInternacion t WHERE t.descripcion = :descripcion")})
public class TipoInternacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id_tipo_internacion")
    private Integer idTipoInternacion;
    @Basic(optional = false)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoInternacion")
    private List<Internados> internadosList;

    public TipoInternacion() {
    }

    public TipoInternacion(Integer idTipoInternacion) {
        this.idTipoInternacion = idTipoInternacion;
    }

    public TipoInternacion(Integer idTipoInternacion, String descripcion) {
        this.idTipoInternacion = idTipoInternacion;
        this.descripcion = descripcion;
    }

    public Integer getIdTipoInternacion() {
        return idTipoInternacion;
    }

    public void setIdTipoInternacion(Integer idTipoInternacion) {
        this.idTipoInternacion = idTipoInternacion;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Internados> getInternadosList() {
        return internadosList;
    }

    public void setInternadosList(List<Internados> internadosList) {
        this.internadosList = internadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoInternacion != null ? idTipoInternacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TipoInternacion)) {
            return false;
        }
        TipoInternacion other = (TipoInternacion) object;
        if ((this.idTipoInternacion == null && other.idTipoInternacion != null) || (this.idTipoInternacion != null && !this.idTipoInternacion.equals(other.idTipoInternacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.TipoInternacion[ idTipoInternacion=" + idTipoInternacion + " ]";
    }
    
}
