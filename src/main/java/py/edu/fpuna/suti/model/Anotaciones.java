/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Anotaciones.findAll", query = "SELECT a FROM Anotaciones a")
    , @NamedQuery(name = "Anotaciones.findByIdAnotacion", query = "SELECT a FROM Anotaciones a WHERE a.idAnotacion = :idAnotacion")
    , @NamedQuery(name = "Anotaciones.findByValor", query = "SELECT a FROM Anotaciones a WHERE a.valor = :valor")
    , @NamedQuery(name = "Anotaciones.findByFechaInsercion", query = "SELECT a FROM Anotaciones a WHERE a.fechaInsercion = :fechaInsercion")
    , @NamedQuery(name = "Anotaciones.findByFechaActualizacion", query = "SELECT a FROM Anotaciones a WHERE a.fechaActualizacion = :fechaActualizacion")})
public class Anotaciones implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_anotacion")
    private Integer idAnotacion;
    @Basic(optional = false)
    private String valor;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @Basic(optional = false)
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_internado", referencedColumnName = "id_internado")
    @ManyToOne(optional = false)
    private Internados idInternado;
    @JoinColumn(name = "id_registrador", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idRegistrador;
    @JoinColumn(name = "id_tipo_anotacion", referencedColumnName = "id_tipo_anotacion")
    @ManyToOne(optional = false)
    private TiposAnotaciones idTipoAnotacion;

    public Anotaciones() {
    }

    public Anotaciones(Integer idAnotacion) {
        this.idAnotacion = idAnotacion;
    }

    public Anotaciones(Integer idAnotacion, String valor, Date fechaInsercion, Date fechaActualizacion) {
        this.idAnotacion = idAnotacion;
        this.valor = valor;
        this.fechaInsercion = fechaInsercion;
        this.fechaActualizacion = fechaActualizacion;
    }

    public Integer getIdAnotacion() {
        return idAnotacion;
    }

    public void setIdAnotacion(Integer idAnotacion) {
        this.idAnotacion = idAnotacion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Internados getIdInternado() {
        return idInternado;
    }

    public void setIdInternado(Internados idInternado) {
        this.idInternado = idInternado;
    }

    public Personas getIdRegistrador() {
        return idRegistrador;
    }

    public void setIdRegistrador(Personas idRegistrador) {
        this.idRegistrador = idRegistrador;
    }

    public TiposAnotaciones getIdTipoAnotacion() {
        return idTipoAnotacion;
    }

    public void setIdTipoAnotacion(TiposAnotaciones idTipoAnotacion) {
        this.idTipoAnotacion = idTipoAnotacion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idAnotacion != null ? idAnotacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Anotaciones)) {
            return false;
        }
        Anotaciones other = (Anotaciones) object;
        if ((this.idAnotacion == null && other.idAnotacion != null) || (this.idAnotacion != null && !this.idAnotacion.equals(other.idAnotacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Anotaciones[ idAnotacion=" + idAnotacion + " ]";
    }
    
}
