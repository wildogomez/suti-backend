/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "medicos_asignados")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MedicosAsignados.findAll", query = "SELECT m FROM MedicosAsignados m")
    , @NamedQuery(name = "MedicosAsignados.findByIdMedicoAsignado", query = "SELECT m FROM MedicosAsignados m WHERE m.idMedicoAsignado = :idMedicoAsignado")
    , @NamedQuery(name = "MedicosAsignados.findByHabilitado", query = "SELECT m FROM MedicosAsignados m WHERE m.habilitado = :habilitado")
    , @NamedQuery(name = "MedicosAsignados.findByFechaAsignacion", query = "SELECT m FROM MedicosAsignados m WHERE m.fechaAsignacion = :fechaAsignacion")
    , @NamedQuery(name = "MedicosAsignados.findByFechaActualizacion", query = "SELECT m FROM MedicosAsignados m WHERE m.fechaActualizacion = :fechaActualizacion")})
public class MedicosAsignados implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_medico_asignado")
    private Integer idMedicoAsignado;
    @Basic(optional = false)
    private boolean habilitado;
    @Basic(optional = false)
    @Column(name = "fecha_asignacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaAsignacion;
    @Column(name = "fecha_actualizacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaActualizacion;
    @JoinColumn(name = "id_internado", referencedColumnName = "id_internado")
    @ManyToOne(optional = false)
    private Internados idInternado;
    @JoinColumn(name = "id_medico", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idMedico;

    public MedicosAsignados() {
    }

    public MedicosAsignados(Integer idMedicoAsignado) {
        this.idMedicoAsignado = idMedicoAsignado;
    }

    public MedicosAsignados(Integer idMedicoAsignado, boolean habilitado, Date fechaAsignacion) {
        this.idMedicoAsignado = idMedicoAsignado;
        this.habilitado = habilitado;
        this.fechaAsignacion = fechaAsignacion;
    }

    public Integer getIdMedicoAsignado() {
        return idMedicoAsignado;
    }

    public void setIdMedicoAsignado(Integer idMedicoAsignado) {
        this.idMedicoAsignado = idMedicoAsignado;
    }

    public boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Date getFechaAsignacion() {
        return fechaAsignacion;
    }

    public void setFechaAsignacion(Date fechaAsignacion) {
        this.fechaAsignacion = fechaAsignacion;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Internados getIdInternado() {
        return idInternado;
    }

    public void setIdInternado(Internados idInternado) {
        this.idInternado = idInternado;
    }

    public Personas getIdMedico() {
        return idMedico;
    }

    public void setIdMedico(Personas idMedico) {
        this.idMedico = idMedico;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idMedicoAsignado != null ? idMedicoAsignado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MedicosAsignados)) {
            return false;
        }
        MedicosAsignados other = (MedicosAsignados) object;
        if ((this.idMedicoAsignado == null && other.idMedicoAsignado != null) || (this.idMedicoAsignado != null && !this.idMedicoAsignado.equals(other.idMedicoAsignado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.MedicosAsignados[ idMedicoAsignado=" + idMedicoAsignado + " ]";
    }
    
}
