/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "tipos_clientes")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TiposClientes.findAll", query = "SELECT t FROM TiposClientes t")
    , @NamedQuery(name = "TiposClientes.findByIdTipoCliente", query = "SELECT t FROM TiposClientes t WHERE t.idTipoCliente = :idTipoCliente")
    , @NamedQuery(name = "TiposClientes.findByDescripcion", query = "SELECT t FROM TiposClientes t WHERE t.descripcion = :descripcion")})
public class TiposClientes implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @Column(name = "id_tipo_cliente")
    private BigDecimal idTipoCliente;
    @Basic(optional = false)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTipoCliente")
    private List<Internados> internadosList;

    public TiposClientes() {
    }

    public TiposClientes(BigDecimal idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    public TiposClientes(BigDecimal idTipoCliente, String descripcion) {
        this.idTipoCliente = idTipoCliente;
        this.descripcion = descripcion;
    }

    public BigDecimal getIdTipoCliente() {
        return idTipoCliente;
    }

    public void setIdTipoCliente(BigDecimal idTipoCliente) {
        this.idTipoCliente = idTipoCliente;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Internados> getInternadosList() {
        return internadosList;
    }

    public void setInternadosList(List<Internados> internadosList) {
        this.internadosList = internadosList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTipoCliente != null ? idTipoCliente.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TiposClientes)) {
            return false;
        }
        TiposClientes other = (TiposClientes) object;
        if ((this.idTipoCliente == null && other.idTipoCliente != null) || (this.idTipoCliente != null && !this.idTipoCliente.equals(other.idTipoCliente))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.TiposClientes[ idTipoCliente=" + idTipoCliente + " ]";
    }
    
}
