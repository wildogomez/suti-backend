/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Articulos.findAll", query = "SELECT a FROM Articulos a")
    , @NamedQuery(name = "Articulos.findByIdArticulo", query = "SELECT a FROM Articulos a WHERE a.idArticulo = :idArticulo")
    , @NamedQuery(name = "Articulos.findByCodigo", query = "SELECT a FROM Articulos a WHERE a.codigo = :codigo")
    , @NamedQuery(name = "Articulos.findByDescripcion", query = "SELECT a FROM Articulos a WHERE a.descripcion = :descripcion")})
public class Articulos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_articulo")
    private Integer idArticulo;
    @Basic(optional = false)
    private String codigo;
    @Basic(optional = false)
    private String descripcion;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArticulo")
    private List<Desechables> desechablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idArticulo")
    private List<DepositosArticulos> depositosArticulosList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idMedicamento")
    private List<MedicamentoInternado> medicamentoInternadoList;
    @JoinColumn(name = "id_tipo_articulo", referencedColumnName = "id_tipo_articulo")
    @ManyToOne(optional = false)
    private TiposArticulo idTipoArticulo;

    public Articulos() {
    }

    public Articulos(Integer idArticulo) {
        this.idArticulo = idArticulo;
    }

    public Articulos(Integer idArticulo, String codigo, String descripcion) {
        this.idArticulo = idArticulo;
        this.codigo = codigo;
        this.descripcion = descripcion;
    }

    public Integer getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Integer idArticulo) {
        this.idArticulo = idArticulo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @XmlTransient
    public List<Desechables> getDesechablesList() {
        return desechablesList;
    }

    public void setDesechablesList(List<Desechables> desechablesList) {
        this.desechablesList = desechablesList;
    }

    @XmlTransient
    public List<DepositosArticulos> getDepositosArticulosList() {
        return depositosArticulosList;
    }

    public void setDepositosArticulosList(List<DepositosArticulos> depositosArticulosList) {
        this.depositosArticulosList = depositosArticulosList;
    }

    @XmlTransient
    public List<MedicamentoInternado> getMedicamentoInternadoList() {
        return medicamentoInternadoList;
    }

    public void setMedicamentoInternadoList(List<MedicamentoInternado> medicamentoInternadoList) {
        this.medicamentoInternadoList = medicamentoInternadoList;
    }

    public TiposArticulo getIdTipoArticulo() {
        return idTipoArticulo;
    }

    public void setIdTipoArticulo(TiposArticulo idTipoArticulo) {
        this.idTipoArticulo = idTipoArticulo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idArticulo != null ? idArticulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Articulos)) {
            return false;
        }
        Articulos other = (Articulos) object;
        if ((this.idArticulo == null && other.idArticulo != null) || (this.idArticulo != null && !this.idArticulo.equals(other.idArticulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Articulos[ idArticulo=" + idArticulo + " ]";
    }
    
}
