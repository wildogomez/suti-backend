/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Félix Gómez
 */
@Entity
@Table(name = "turno_enfermero")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "TurnoEnfermero.findAll", query = "SELECT t FROM TurnoEnfermero t")
    , @NamedQuery(name = "TurnoEnfermero.findByIdTurnoEnfermero", query = "SELECT t FROM TurnoEnfermero t WHERE t.idTurnoEnfermero = :idTurnoEnfermero")
    , @NamedQuery(name = "TurnoEnfermero.findByHabilitado", query = "SELECT t FROM TurnoEnfermero t WHERE t.habilitado = :habilitado")
    , @NamedQuery(name = "TurnoEnfermero.findByFechaInsercion", query = "SELECT t FROM TurnoEnfermero t WHERE t.fechaInsercion = :fechaInsercion")})
public class TurnoEnfermero implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_turno_enfermero")
    private Integer idTurnoEnfermero;
    @Basic(optional = false)
    private boolean habilitado;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @JoinColumn(name = "id_enfermero", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idEnfermero;
    @JoinColumn(name = "id_turno", referencedColumnName = "id_turno")
    @ManyToOne(optional = false)
    private Turnos idTurno;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTurnoEnfermero")
    private List<Desechables> desechablesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "idTurnoEnfermero")
    private List<RegistroMedico> registroMedicoList;

    public TurnoEnfermero() {
    }

    public TurnoEnfermero(Integer idTurnoEnfermero) {
        this.idTurnoEnfermero = idTurnoEnfermero;
    }

    public TurnoEnfermero(Integer idTurnoEnfermero, boolean habilitado, Date fechaInsercion) {
        this.idTurnoEnfermero = idTurnoEnfermero;
        this.habilitado = habilitado;
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdTurnoEnfermero() {
        return idTurnoEnfermero;
    }

    public void setIdTurnoEnfermero(Integer idTurnoEnfermero) {
        this.idTurnoEnfermero = idTurnoEnfermero;
    }

    public boolean getHabilitado() {
        return habilitado;
    }

    public void setHabilitado(boolean habilitado) {
        this.habilitado = habilitado;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Personas getIdEnfermero() {
        return idEnfermero;
    }

    public void setIdEnfermero(Personas idEnfermero) {
        this.idEnfermero = idEnfermero;
    }

    public Turnos getIdTurno() {
        return idTurno;
    }

    public void setIdTurno(Turnos idTurno) {
        this.idTurno = idTurno;
    }

    @XmlTransient
    public List<Desechables> getDesechablesList() {
        return desechablesList;
    }

    public void setDesechablesList(List<Desechables> desechablesList) {
        this.desechablesList = desechablesList;
    }

    @XmlTransient
    public List<RegistroMedico> getRegistroMedicoList() {
        return registroMedicoList;
    }

    public void setRegistroMedicoList(List<RegistroMedico> registroMedicoList) {
        this.registroMedicoList = registroMedicoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idTurnoEnfermero != null ? idTurnoEnfermero.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof TurnoEnfermero)) {
            return false;
        }
        TurnoEnfermero other = (TurnoEnfermero) object;
        if ((this.idTurnoEnfermero == null && other.idTurnoEnfermero != null) || (this.idTurnoEnfermero != null && !this.idTurnoEnfermero.equals(other.idTurnoEnfermero))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.TurnoEnfermero[ idTurnoEnfermero=" + idTurnoEnfermero + " ]";
    }
    
}
