/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package py.edu.fpuna.suti.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Félix Gómez
 */
@Entity
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Desechables.findAll", query = "SELECT d FROM Desechables d")
    , @NamedQuery(name = "Desechables.findByIdDesechableUtilizado", query = "SELECT d FROM Desechables d WHERE d.idDesechableUtilizado = :idDesechableUtilizado")
    , @NamedQuery(name = "Desechables.findByCantidad", query = "SELECT d FROM Desechables d WHERE d.cantidad = :cantidad")
    , @NamedQuery(name = "Desechables.findByFechaInsercion", query = "SELECT d FROM Desechables d WHERE d.fechaInsercion = :fechaInsercion")})
public class Desechables implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_desechable_utilizado")
    private Integer idDesechableUtilizado;
    @Basic(optional = false)
    private int cantidad;
    @Basic(optional = false)
    @Column(name = "fecha_insercion")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaInsercion;
    @JoinColumn(name = "id_articulo", referencedColumnName = "id_articulo")
    @ManyToOne(optional = false)
    private Articulos idArticulo;
    @JoinColumn(name = "id_internado", referencedColumnName = "id_internado")
    @ManyToOne(optional = false)
    private Internados idInternado;
    @JoinColumn(name = "id_enfermero", referencedColumnName = "id_persona")
    @ManyToOne(optional = false)
    private Personas idEnfermero;
    @JoinColumn(name = "id_turno_enfermero", referencedColumnName = "id_turno_enfermero")
    @ManyToOne(optional = false)
    private TurnoEnfermero idTurnoEnfermero;

    public Desechables() {
    }

    public Desechables(Integer idDesechableUtilizado) {
        this.idDesechableUtilizado = idDesechableUtilizado;
    }

    public Desechables(Integer idDesechableUtilizado, int cantidad, Date fechaInsercion) {
        this.idDesechableUtilizado = idDesechableUtilizado;
        this.cantidad = cantidad;
        this.fechaInsercion = fechaInsercion;
    }

    public Integer getIdDesechableUtilizado() {
        return idDesechableUtilizado;
    }

    public void setIdDesechableUtilizado(Integer idDesechableUtilizado) {
        this.idDesechableUtilizado = idDesechableUtilizado;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaInsercion() {
        return fechaInsercion;
    }

    public void setFechaInsercion(Date fechaInsercion) {
        this.fechaInsercion = fechaInsercion;
    }

    public Articulos getIdArticulo() {
        return idArticulo;
    }

    public void setIdArticulo(Articulos idArticulo) {
        this.idArticulo = idArticulo;
    }

    public Internados getIdInternado() {
        return idInternado;
    }

    public void setIdInternado(Internados idInternado) {
        this.idInternado = idInternado;
    }

    public Personas getIdEnfermero() {
        return idEnfermero;
    }

    public void setIdEnfermero(Personas idEnfermero) {
        this.idEnfermero = idEnfermero;
    }

    public TurnoEnfermero getIdTurnoEnfermero() {
        return idTurnoEnfermero;
    }

    public void setIdTurnoEnfermero(TurnoEnfermero idTurnoEnfermero) {
        this.idTurnoEnfermero = idTurnoEnfermero;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idDesechableUtilizado != null ? idDesechableUtilizado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Desechables)) {
            return false;
        }
        Desechables other = (Desechables) object;
        if ((this.idDesechableUtilizado == null && other.idDesechableUtilizado != null) || (this.idDesechableUtilizado != null && !this.idDesechableUtilizado.equals(other.idDesechableUtilizado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "py.edu.fpuna.suti.model.Desechables[ idDesechableUtilizado=" + idDesechableUtilizado + " ]";
    }
    
}
