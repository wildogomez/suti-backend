package py.edu.fpuna.suti;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SutiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SutiApplication.class, args);
	}

}
