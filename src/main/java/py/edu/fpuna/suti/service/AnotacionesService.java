package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Anotaciones;
import py.edu.fpuna.suti.repo.IAnotaciones;

@Service
public class AnotacionesService {

	
	private static Logger log = LoggerFactory.getLogger(AnotacionesService.class);
	
	@Autowired
	private IAnotaciones repoAnotaciones;
	
	public List<Anotaciones> getAnotaciones() {

		List<Anotaciones> anotaciones = null;

		try {
			anotaciones = repoAnotaciones.findAll();
			return anotaciones;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	public Anotaciones postAnotacion(Anotaciones newAnotacion) {		
		try {
			repoAnotaciones.save(newAnotacion);
			return newAnotacion;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
}
