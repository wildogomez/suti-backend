package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Anotaciones;
import py.edu.fpuna.suti.model.Salas;
import py.edu.fpuna.suti.repo.ISalas;

@Service
public class SalasService {
	
	private static Logger log = LoggerFactory.getLogger(SalasService.class);
	
	@Autowired
	private ISalas repoSalas;
	
	public List<Salas> getSalas() {

		List<Salas> salas = null;

		try {
			salas = repoSalas.findAll();
			return salas;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	public Salas postSalas(Salas newSala) {		
		try {
			repoSalas.save(newSala);
			return newSala;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}

}
