package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Articulos;
import py.edu.fpuna.suti.repo.IArticulos;

@Service
public class ArticulosService {
	
	private static Logger log = LoggerFactory.getLogger(ArticulosService.class);
	
	@Autowired
	private IArticulos repoArticulos;

	public List<Articulos> getArticulos() {
		List<Articulos> articulos = null;
		try {
			articulos = repoArticulos.findAll();
			return articulos;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
		
	public Articulos postArticulo(Articulos newArticulo) {		
		try {
			repoArticulos.save(newArticulo);
			return newArticulo;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
}
