package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.repo.IDepositos;

@Service
public class DepositosService {

	private static Logger log = LoggerFactory.getLogger(DepositosService.class);
	
	@Autowired
	private IDepositos repoDepositos;
	
	public List<Depositos> getDepositos() {
		
		List<Depositos> depositos = null;
		
		try {
			depositos = repoDepositos.findAll();
			return depositos;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	public Depositos postDeposito(Depositos newDeposito) {		
		try {
			repoDepositos.save(newDeposito);
			return newDeposito;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}


}
