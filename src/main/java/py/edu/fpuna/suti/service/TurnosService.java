package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.model.Turnos;
import py.edu.fpuna.suti.repo.IDepositos;
import py.edu.fpuna.suti.repo.ITurnos;

@Service
public class TurnosService {
	
	private static Logger log = LoggerFactory.getLogger(TurnosService.class);
	
	@Autowired
	private ITurnos repoTurnos;
	
	public List<Turnos> getTurnos() {
		
		List<Turnos> turnos = null;
		
		try {
			turnos = repoTurnos.findAll();
			return turnos;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	public Turnos postTurno(Turnos newTurno) {		
		try {
			repoTurnos.save(newTurno);
			return newTurno;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}

}
