package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Categorias;
import py.edu.fpuna.suti.repo.ICategorias;

@Service
public class CategoriasService {

	private static Logger log = LoggerFactory.getLogger(CategoriasService.class);

	@Autowired
	private ICategorias repoCategorias;

	public List<Categorias> getCategorias() {

		List<Categorias> categorias = null;

		try {
			categorias = repoCategorias.findAll();
			return categorias;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	public Categorias postCategoria(Categorias newCategoria) {		
		try {
			repoCategorias.save(newCategoria);
			return newCategoria;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}

}
