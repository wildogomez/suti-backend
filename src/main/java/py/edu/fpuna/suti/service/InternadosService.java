package py.edu.fpuna.suti.service;

import java.util.List;
import java.util.NoSuchElementException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.Internados;
import py.edu.fpuna.suti.model.RegistroMedico;
import py.edu.fpuna.suti.repo.IInternados;
import py.edu.fpuna.suti.repo.IRegistroMedico;

@Service
public class InternadosService {

	private static Logger log = LoggerFactory.getLogger(InternadosService.class);

	@Autowired
	private IInternados repoInternados;
	@Autowired
	private IRegistroMedico repoRegistroMedico;
	
	
	public Internados getInternado(int id) {
		
		Internados internado = null;
		
		try {
			internado = repoInternados.findById(id).get();
			return internado;
		} catch (NoSuchElementException e) {
			log.error("No se ha encontrado internado con ID: " + String.valueOf(id));
			throw e;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		} 
	}
	
	
	public List<Internados> getInternados() {
		
		List<Internados> internados = null;
		
		try {
			internados = repoInternados.findAll();
			return internados;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	
	public Internados postInternados(Internados newInternado) {		
		try {
			repoInternados.save(newInternado);
			return newInternado;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	
	public List<RegistroMedico> getInternadoRegistros(
			int id,
			String fechaHoraInicio,
			String fechaHoraFin) {
		
		List<RegistroMedico> registros = null;
		
		try {
			registros = repoRegistroMedico.findAll();
			return registros;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}


}
