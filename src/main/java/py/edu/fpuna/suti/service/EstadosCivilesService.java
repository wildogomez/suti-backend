package py.edu.fpuna.suti.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import py.edu.fpuna.suti.model.EstadosCiviles;
import py.edu.fpuna.suti.repo.IEstadosCiviles;

@Service
public class EstadosCivilesService {

	private static Logger log = LoggerFactory.getLogger(EstadosCivilesService.class);
	
	@Autowired
	private IEstadosCiviles repoEstadosCiviles;
	
	public List<EstadosCiviles> getEstadosCiviles() {
		List<EstadosCiviles> estadosCiviles = null;
		try {
			estadosCiviles = repoEstadosCiviles.findAll();
			return estadosCiviles;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	public EstadosCiviles postEstadoCivil(EstadosCiviles newEstadoCivil) {		
		try {
			repoEstadosCiviles.save(newEstadoCivil);
			return newEstadoCivil;
		} catch (Exception e) {
			log.error("Ha ocurrido un error: " + e.toString());
			throw e;
		}
	}
	
	
	
	
}
