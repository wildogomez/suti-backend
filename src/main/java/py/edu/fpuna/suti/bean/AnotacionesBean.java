package py.edu.fpuna.suti.bean;

import java.sql.Date;

import py.edu.fpuna.suti.model.Personas;
import py.edu.fpuna.suti.model.TiposAnotaciones;

public class AnotacionesBean {
	
	private int id_anotacion;
	private Personas id_registrador;
	private TiposAnotaciones id_tipo_anotacion;
	private String valor;
	private Date fecha_insercion;
	private Date fecha_actualizacion;
	private String id_internado;

	public Personas getId_registrador() {
		return id_registrador;
	}
	public void setId_registrador(Personas id_registrador) {
		this.id_registrador = id_registrador;
	}
	public int getId_anotacion() {
		return id_anotacion;
	}
	public void setId_anotacion(int id_anotacion) {
		this.id_anotacion = id_anotacion;
	}
	
	
	
	public TiposAnotaciones getId_tipo_anotacion() {
		return id_tipo_anotacion;
	}
	public void setId_tipo_anotacion(TiposAnotaciones id_tipo_anotacion) {
		this.id_tipo_anotacion = id_tipo_anotacion;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public Date getFecha_insercion() {
		return fecha_insercion;
	}
	public void setFecha_insercion(Date fecha_insercion) {
		this.fecha_insercion = fecha_insercion;
	}
	public Date getFecha_actualizacion() {
		return fecha_actualizacion;
	}
	public void setFecha_actualizacion(Date fecha_actualizacion) {
		this.fecha_actualizacion = fecha_actualizacion;
	}
	public String getId_internado() {
		return id_internado;
	}
	public void setId_internado(String id_internado) {
		this.id_internado = id_internado;
	}
	

}
