package py.edu.fpuna.suti.bean;

public class SalasBean {
	
	
	private int	 id_sala;
	private String descripcion;
	private String numero_sala;
	private boolean habilitado;
	public int getId_sala() {
		return id_sala;
	}
	public void setId_sala(int id_sala) {
		this.id_sala = id_sala;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public String getNumero_sala() {
		return numero_sala;
	}
	public void setNumero_sala(String numero_sala) {
		this.numero_sala = numero_sala;
	}
	public boolean isHabilitado() {
		return habilitado;
	}
	public void setHabilitado(boolean habilitado) {
		this.habilitado = habilitado;
	}
		
}
