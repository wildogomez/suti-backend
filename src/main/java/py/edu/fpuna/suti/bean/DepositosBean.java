package py.edu.fpuna.suti.bean;

public class DepositosBean {
    
    private int idDeposito;
    private String codigo;
    private String descripcion;
    
	public int getIdDeposito() {
		return idDeposito;
	}
	
	public void setIdDeposito(int idDeposito) {
		this.idDeposito = idDeposito;
	}
	
	public String getCodigo() {
		return codigo;
	}
	
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public String toString() {
		return "DepositosBean [idDeposito=" + idDeposito + ", codigo=" + codigo + ", descripcion=" + descripcion + "]";
	}
    
}