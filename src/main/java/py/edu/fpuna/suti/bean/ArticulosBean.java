package py.edu.fpuna.suti.bean;

import py.edu.fpuna.suti.model.TiposArticulo;

public class ArticulosBean {

	private int id_articulo;
	private String codigo;
	private TiposArticulo id_tipo_articulo;
	private String descripcion;
	
	
	
	public TiposArticulo getId_tipo_articulo() {
		return id_tipo_articulo;
	}
	public void setId_tipo_articulo(TiposArticulo id_tipo_articulo) {
		this.id_tipo_articulo = id_tipo_articulo;
	}
	public int getId_articulo() {
		return id_articulo;
	}
	public void setId_articulo(int id_articulo) {
		this.id_articulo = id_articulo;
	}
	public String getCodigo() {
		return codigo;
	}
	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
