package py.edu.fpuna.suti.bean;

public class EstadosCivilesBean {
	
	private int id_estado_civil;
	private String descripcion;
	
	public int getId_estado_civil() {
		return id_estado_civil;
	}
	public void setId_estado_civil(int id_estado_civil) {
		this.id_estado_civil = id_estado_civil;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
