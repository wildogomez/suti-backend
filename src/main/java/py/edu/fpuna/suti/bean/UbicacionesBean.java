package py.edu.fpuna.suti.bean;

public class UbicacionesBean {

	private int id_ubicacion;
	private String pais;
	private String departamento;
	private String ciudad;
	private String barrio;
	
	
	public int getId_ubicacion() {
		return id_ubicacion;
	}
	public void setId_ubicacion(int id_ubicacion) {
		this.id_ubicacion = id_ubicacion;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getDepartamento() {
		return departamento;
	}
	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	public String getCiudad() {
		return ciudad;
	}
	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}
	public String getBarrio() {
		return barrio;
	}
	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}
}
