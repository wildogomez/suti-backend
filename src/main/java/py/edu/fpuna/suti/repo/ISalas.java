package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Salas;

public interface ISalas extends JpaRepository<Salas, Integer>{
	
	
}
