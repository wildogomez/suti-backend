package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.RegistroMedico;

public interface IRegistroMedico extends JpaRepository<RegistroMedico, Integer>{
	
	
}
