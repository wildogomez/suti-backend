package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Categorias;

public interface ICategorias extends JpaRepository<Categorias, Integer>{

}
