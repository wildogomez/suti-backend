package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Articulos;

public interface IArticulos extends JpaRepository<Articulos, Integer>{

}
