package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Turnos;

public interface ITurnos extends JpaRepository<Turnos, Integer>{

}
