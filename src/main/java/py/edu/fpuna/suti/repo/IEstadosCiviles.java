package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Depositos;
import py.edu.fpuna.suti.model.EstadosCiviles;

public interface IEstadosCiviles extends JpaRepository<EstadosCiviles, Integer>{

}
