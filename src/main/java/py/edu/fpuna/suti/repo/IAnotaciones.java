package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Anotaciones;

public interface IAnotaciones extends JpaRepository<Anotaciones, Integer>{

}
