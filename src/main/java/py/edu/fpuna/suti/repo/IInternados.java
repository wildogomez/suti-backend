package py.edu.fpuna.suti.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import py.edu.fpuna.suti.model.Internados;

public interface IInternados extends JpaRepository<Internados, Integer>{
	
	
}
